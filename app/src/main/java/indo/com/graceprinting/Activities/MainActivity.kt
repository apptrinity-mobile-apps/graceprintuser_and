package indo.com.graceprinting.Activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import com.indobytes.a4printuser.Helper.CustomTextViewSemiBold
import indo.com.graceprinting.Adapter.MyPagerAdapter
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.R
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var drawer: DrawerLayout
    internal lateinit var sessionManager: SessionManager
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    internal lateinit var department_name: String
    internal lateinit var department_id: String
    internal lateinit var printer_id: String
    internal val PERMISSION_REQUEST_CODE = 111
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sessionManager = SessionManager(this)

//viewPager.setOffscreenPageLimit(0);
        sessionManager.checkLogin()

        //if(sessionManager.userDetails != "")
        val user = sessionManager.userDetails

        user_name = user.get(SessionManager.PROFILE_USERNAME).toString()
        dept_name = user.get(SessionManager.DEPARTMENTNAME).toString()

        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val toolbar_title = findViewById<CustomTextViewSemiBold>(R.id.toolbar_title)

        //department_id = user.get(SessionManager.DEPARTMENTID).toString()

        if(intent != null && intent.getStringExtra("department_id")!=null){
            department_id = intent.getStringExtra("department_id")
        }
        if(intent != null && intent.getStringExtra("department_name")!=null){
            department_name = intent.getStringExtra("department_name")
        }
        if(intent != null && intent.getStringExtra("printerid")!=null){
            printer_id = intent.getStringExtra("printerid")
        }
        if(department_name == "Press" && printer_id == ""){
            val intent = Intent(this, PressPrinterTypesActivity::class.java)
            intent.putExtra("department_id", department_id)
            intent.putExtra("department_name", department_name)
            startActivity(intent)
        }

        toolbar_title.setText(department_name)

        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close)

        drawer.setDrawerListener(toggle)
        toggle.syncState()


        toggle.setDrawerIndicatorEnabled(true);
        //toggle.setHomeAsUpIndicator(R.drawable.ic_launcher_background);

        toggle.setToolbarNavigationClickListener {

            drawer.openDrawer(GravityCompat.START);
        }



        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter
        viewpager_main.setOffscreenPageLimit(0)
        tabs_main.setupWithViewPager(viewpager_main)

        tabs_main.getTabAt(0)!!.setCustomView(R.layout.tab1);
        tabs_main.getTabAt(1)!!.setCustomView(R.layout.tab2);
        tabs_main.getTabAt(2)!!.setCustomView(R.layout.tab3);
        tabs_main.getTabAt(3)!!.setCustomView(R.layout.tab4);


        tabs_main.getTabAt(0)!!.setIcon(resources.getDrawable(R.drawable.tab_down_icon))

        Log.e("tabcount", "" + tabs_main.tabCount);

        Log.e("DEPT", department_name+"" + user_name+"-----"+dept_name);

        val header = navigationView.getHeaderView(0)
        val username = header.findViewById(R.id.tv_username) as TextView
        val deptname = header.findViewById(R.id.tv_department_id) as TextView
        username!!.setText(user_name)
        deptname!!.setText(dept_name)




        /*if(intent != null && intent.getStringExtra("department_id")!=null){
            department_id = intent.getStringExtra("department_id")
        }*/
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission()
            } else {

            }
        } else {

        }

    }
    private fun checkReadExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkWriteExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
    }
    override fun onBackPressed() {
        Log.e("printer_id baclpress", "" + printer_id);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if(printer_id != "") {
                val intent = Intent(this, PressPrinterTypesActivity::class.java)
                intent.putExtra("department_id", department_id)
                intent.putExtra("department_name", department_name)
                startActivity(intent)
            }else{
                val intent = Intent(this, DepartmentListActivity::class.java)
                startActivity(intent)
            }
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                val intent = Intent(this,DepartmentListActivity::class.java)
                startActivity(intent)
            }
           /* R.id.nav_main ->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }*/
            R.id.nav_logout -> {
                sessionManager.logoutUser()
            }

        }

        drawer.closeDrawer(GravityCompat.START)

        return true
    }

    // Extension function to show toast message easily
    private fun Context.toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}
