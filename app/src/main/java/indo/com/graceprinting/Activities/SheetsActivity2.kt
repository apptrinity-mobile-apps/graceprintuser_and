package indo.com.graceprinting.Activities


import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprinting.Helper.ConnectionManager
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.Model.APIResponse.CommentsListDataResponse
import indo.com.graceprinting.Model.APIResponse.PartiallydonDataArray
import indo.com.graceprinting.Model.APIResponse.StartJobListDataResponse
import indo.com.graceprinting.Model.APIResponse.StartJobResponse
import indo.com.graceprinting.R
import retrofit2.Call
import retrofit2.Callback
import java.util.*


class SheetsActivity2 : AppCompatActivity() {
    internal lateinit var sessionManager: SessionManager
    internal lateinit var my_loader: Dialog
    var job_id: String? = null
    lateinit var assigned_id: String
    lateinit var job_department_id: String
    internal  var assigned_task_id: String =""
    lateinit var allotted_sheets: String
    lateinit var used_sheets: String
    lateinit var department_id: String
    internal var  printertype: String = ""
    internal var  optiontype: String =  ""
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataChild: HashMap<String, ArrayList<StartJobListDataResponse>>
    internal lateinit var user_id: String
    internal lateinit var comments_text: String
    internal lateinit var total_sheets: String
    internal  var assign_task_id: String =""
    internal var ll_comments: LinearLayout? = null
    internal var tv_comments: CustomTextViewBold? = null
    lateinit var active_continue: String
    lateinit var et_total_sheets: EditText

    lateinit var tv_alloshts_id: CustomTextViewBold
    lateinit var btn_postcomment: CustomTextViewBold
    lateinit var btn_done: CustomTextViewBold
    lateinit var btn_par_done: CustomTextViewBold

    lateinit var tv_exshts_id: CustomTextViewBold
    lateinit var et_comments: EditText
    ///partial sheets
    lateinit var ll_s_no_id:LinearLayout
    lateinit var ll_sheets_count_id:LinearLayout
    lateinit var ll_sheets_list_id:LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)

        setContentView(R.layout.activity_sheets)
        myloading()


        sessionManager = SessionManager(this!!)
        val user = sessionManager.userDetails
        user_id = user.get(SessionManager.PROFILE_ID).toString()
        Log.e("user_id  2", user_id);

        job_id = intent.getStringExtra("job_id")
        assigned_id = intent.getStringExtra("assigned_id")
        job_department_id = intent.getStringExtra("job_department_id")
       // assigned_task_id = intent.getStringExtra("assigned_task_id")
        allotted_sheets = intent.getStringExtra("allotted_sheets")
        used_sheets = intent.getStringExtra("used_sheets")

        department_id = intent.getStringExtra("department_id")
        printertype = intent.getStringExtra("printer_type")?: ""
        optiontype = intent.getStringExtra("option_type")

        Log.e("user_id  2", printertype+"-0-0-0-"+optiontype);
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val tv_job_id_value = findViewById(R.id.tv_job_id_value) as CustomTextViewBold
        var rootview = findViewById(R.id.ll_login_id) as LinearLayout

         tv_alloshts_id = findViewById(R.id.tv_alloshts_id) as CustomTextViewBold
        et_total_sheets = findViewById(R.id.et_total_sheets) as EditText
         tv_exshts_id = findViewById(R.id.tv_exshts_id) as CustomTextViewBold
        tv_comments = findViewById(R.id.tv_comments) as CustomTextViewBold
         et_comments = findViewById(R.id.et_comments) as EditText
         btn_postcomment = findViewById(R.id.btn_postcomment) as CustomTextViewBold
         btn_done = findViewById(R.id.btn_done) as CustomTextViewBold
        ll_comments = findViewById(R.id.ll_comments) as LinearLayout
        btn_par_done = findViewById(R.id.btn_par_done) as CustomTextViewBold

        ll_s_no_id = findViewById(R.id.ll_s_no_id) as LinearLayout
        ll_sheets_count_id = findViewById(R.id.ll_sheets_count_id) as LinearLayout
        ll_sheets_list_id = findViewById(R.id.ll_sheets_list_id) as LinearLayout

        tv_job_id_value.setText("Job -" +job_id)
        tv_alloshts_id.setText(allotted_sheets)
        et_total_sheets.setText(used_sheets)

        val tv_printertype = findViewById(R.id.tv_printertype) as CustomTextViewBold
        tv_printertype.setText(printertype)
        val tv_optiontype = findViewById(R.id.tv_optiontype) as CustomTextViewBold
        tv_optiontype.setText(optiontype)

        val dsds = (used_sheets.toInt() - allotted_sheets.toInt())

        Log.d("SHEETSUSED&ALLOTTED", allotted_sheets + "----" + used_sheets + "------" + dsds)
        tv_exshts_id.setText(dsds.toString())

        et_total_sheets.isFocusable = false
        btn_postcomment.visibility = View.GONE
        et_comments.visibility = View.GONE
        btn_done.visibility = View.GONE
        btn_par_done.visibility = View.GONE

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            val home_intent = Intent(this, DepartmentListActivity::class.java)
           // intent.putExtra("department_id",department_id)
            startActivity(home_intent)

           // onBackPressed()
        }
        toolbar.setTitle("SHEETS")

        callstartJobAPI()


        active_continue = intent.getStringExtra("active_continue")
        if (active_continue.equals("continue")) {
            if (ConnectionManager.checkConnection(applicationContext)) {
                callsingleJobAPI()
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, applicationContext)
            }
        } else if (active_continue.equals("active")) {

            if (ConnectionManager.checkConnection(applicationContext)) {
                callstartJobAPI()
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, applicationContext)
            }


        }

        btn_postcomment.setOnClickListener {

            comments_text = et_comments!!.text.toString()

            if (comments_text.equals("")) {
                Toast.makeText(getApplicationContext(), "Please Enter Comment", Toast.LENGTH_SHORT).show();
            } else {

                Log.d("COMMENTSAPI", user_id + "----" + comments_text + "------" + assigned_id)
                //callpostCommentsAPI()
                callpostCommentsAPI(user_id, comments_text, assigned_id)
            }


        }

        btn_done.setOnClickListener {
            total_sheets = et_total_sheets!!.text.toString()

            if (total_sheets.equals("")) {
                Toast.makeText(getApplicationContext(), "Please Enter Sheets Number", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("DONESHEETS", total_sheets + "------" + assigned_task_id)
                callpostDoneTaskAPI()
            }

        }

    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun callstartJobAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.startJob(assigned_id,job_department_id)
        Log.d("REQUEST", call.toString() + "-----" + assigned_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        listDataHeader = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<StartJobListDataResponse>? = response.body()!!.data!!
                        if (list!!.size > 0) {
                            for (j in 0 until list.size) {

                                listDataHeader.add(list.get(j).task_name.toString())
                                assign_task_id = list.get(j).assigned_task_id.toString()
                                //listDataHeader.add(list.get(j).assigned_task_id.toString())

                                val top = ArrayList<StartJobListDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).toString())

                                listDataChild.put(listDataHeader[j], top)

                                ll_comments?.removeAllViews()
                                if (list.get(j).comments!!.size!! > 0) {
                                    val list: Array<CommentsListDataResponse>? = list.get(j).comments
                                    if (list!!.size > 0) {
                                        for (j in 0 until list.size) {
                                            Log.d("COMMENTSLIST", "" + list!!.size)
                                            /*val textView = TextView(applicationContext)
                                            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                            params!!.gravity = Gravity.END
                                            params.setMargins(5, 5, 5, 5)
                                            textView.setPadding(2, 2, 2, 2)
                                            textView!!.setLayoutParams(params)
                                            textView!!.gravity = Gravity.RIGHT
                                            textView!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                            textView!!.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                            textView!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                            textView!!.setBackground(resources.getDrawable(R.drawable.bubble2))
                                            textView!!.text = list.get(j).comment
                                            ll_comments?.addView(textView)*/

                                            /*if (list.get(j).user_type.equals("admin")) {
                                                val textViewadm = TextView(applicationContext)
                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.START
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                textViewadm.setPadding(2, 2, 2, 2)
                                                textViewadm!!.setLayoutParams(paramsadm)
                                                textViewadm!!.gravity = Gravity.LEFT
                                                textViewadm!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textViewadm!!.setBackground(resources.getDrawable(R.drawable.bubble1))
                                                textViewadm!!.text = list.get(j).comment
                                                ll_comments?.addView(textViewadm)
                                                ll_comments!!.gravity = Gravity.LEFT
                                            }else if(list.get(j).user_type.equals("user")){
                                                val textView = TextView(applicationContext)
                                                // LinearLayout.LayoutParams  = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                params!!.gravity = Gravity.START
                                                params.setMargins(5, 5, 5, 5)
                                                textView.setPadding(2, 2, 2, 2)
                                                textView!!.setLayoutParams(params)
                                                textView!!.gravity = Gravity.LEFT
                                                textView!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textView!!.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                                textView!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textView!!.setBackground(resources.getDrawable(R.drawable.bubble1))
                                                textView!!.text = list.get(j).comment
                                                ll_comments?.addView(textView)
                                            }*/


                                          /*  if (list.get(j).user_type.equals("admin")) {
                                                *//*val textViewadm = TextView(applicationContext)
                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.LEFT
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                textViewadm.setPadding(2, 2, 2, 2)
                                                textViewadm!!.setLayoutParams(paramsadm)
                                                textViewadm!!.gravity = Gravity.LEFT
                                                textViewadm!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textViewadm!!.setBackground(resources.getDrawable(R.drawable.bubble1))
                                                textViewadm!!.text = list.get(j).comment
                                                childViewHolder!!.ll_comments?.addView(textViewadm)*//*


                                                val textdrops = TextView(applicationContext)
                                                val image = ImageView(applicationContext)

                                                val ll_main = LinearLayout(applicationContext)
                                                val ll_name = LinearLayout(applicationContext)

                                                ll_name.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                                                ll_name.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_name.gravity = Gravity.LEFT
                                                ll_name.setPadding(10, 10, 10, 10)

                                                ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                                ll_main.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_main.gravity = Gravity.LEFT
                                                ll_main.setPadding(10, 10, 10, 10)


                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.LEFT
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                paramsadm.gravity = Gravity.LEFT
                                                textdrops!!.setLayoutParams(paramsadm)
                                                image!!.setLayoutParams(paramsadm)

                                                textdrops.setPadding(15, 15, 15, 15)
                                                textdrops!!.setLayoutParams(paramsadm)
                                                textdrops!!.gravity = Gravity.LEFT
                                                textdrops!!.setBackgroundColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textdrops!!.setBackground(resources.getDrawable(R.drawable.chat_bg))
                                                textdrops!!.text = list.get(j).comment


                                                image.layoutParams = android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                image.setImageDrawable(resources.getDrawable(R.drawable.admin_chat_icon))

                                                ll_main.addView(image)
                                                ll_name.addView(textdrops)

                                                ll_main.addView(ll_name)

                                                ll_comments?.addView(ll_main)

                                            } else if (list.get(j).user_type.equals("user")) {
                                                *//*val textView = TextView(applicationContext)
                                                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT)
                                                params!!.gravity = Gravity.RIGHT
                                                params.setMargins(5, 5, 5, 5)
                                                textView.setPadding(2, 2, 2, 2)
                                                textView!!.setLayoutParams(params)
                                                textView!!.gravity = Gravity.LEFT
                                                textView!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textView!!.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                                textView!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textView!!.setBackground(resources.getDrawable(R.drawable.bubble2))
                                                textView!!.text = list.get(j).comment
                                                //ll_comments?.addView(textView)
                                                // textViewadm
                                                childViewHolder!!.ll_comments?.addView(textView)*//*



                                                val textdrops = TextView(applicationContext)
                                                val image = ImageView(applicationContext)

                                                val ll_main = LinearLayout(applicationContext)
                                                val ll_name = LinearLayout(applicationContext)

                                                ll_name.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                                                ll_name.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_name.gravity = Gravity.RIGHT
                                                ll_name.setPadding(10, 10, 10, 10)

                                                ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                                ll_main.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_main.gravity = Gravity.RIGHT
                                                ll_main.setPadding(10, 10, 10, 10)


                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.RIGHT
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                paramsadm.gravity = Gravity.RIGHT
                                                textdrops!!.setLayoutParams(paramsadm)
                                                image!!.setLayoutParams(paramsadm)

                                                textdrops.setPadding(15, 15, 15, 15)
                                                textdrops!!.setLayoutParams(paramsadm)
                                                textdrops!!.gravity = Gravity.RIGHT
                                                textdrops!!.setBackgroundColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textdrops!!.setBackground(resources.getDrawable(R.drawable.chat_bg_user))
                                                textdrops!!.text = list.get(j).comment


                                                image.layoutParams = android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                image.setImageDrawable(resources.getDrawable(R.drawable.user_chat_icon))

                                                ll_main.addView(image)
                                                ll_name.addView(textdrops)

                                                ll_main.addView(ll_name)

                                                ll_comments?.addView(ll_main)



                                            }*/


                                            if (list.get(j).user_type.equals("admin")) {

                                                val a =  LinearLayout(this@SheetsActivity2);
                                                a.setOrientation(LinearLayout.HORIZONTAL);
                                                a.setLayoutParams(LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                                a.setPadding(10, 10, 10, 10)
                                                val textView1 =  TextView(this@SheetsActivity2);

                                                val image = ImageView(this@SheetsActivity2)
                                                image.setImageDrawable(this@SheetsActivity2.resources.getDrawable(R.drawable.admin_chat_icon));
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                textView1!!.setBackground(this@SheetsActivity2.resources.getDrawable(R.drawable.chat_bg))
                                                textView1!!.setTextColor(this@SheetsActivity2.resources.getColor(R.color.black)) // hex color 0xAARRGGBB
                                                textView1!!.text = list.get(j).comment
                                                a.addView(image)
                                                a.addView(textView1)

                                                ll_comments!!.addView(a);

                                            } else if (list.get(j).user_type.equals("user")) {

                                                val a =  LinearLayout(this@SheetsActivity2);
                                                a.setOrientation(LinearLayout.HORIZONTAL);
                                                val textView2 =  TextView(this@SheetsActivity2);
                                                val layoutParams =  LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)


                                                layoutParams.gravity = Gravity.RIGHT;
                                                a.setPadding(10, 10, 10, 10); // (left, top, right, bottom)
                                                a.setLayoutParams(layoutParams);
                                                val image = ImageView(this@SheetsActivity2)
                                                image.setImageDrawable(this@SheetsActivity2.resources.getDrawable(R.drawable.user_chat_icon));
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                textView2!!.setBackground(this@SheetsActivity2.resources.getDrawable(R.drawable.chat_bg))
                                                textView2!!.setTextColor(this@SheetsActivity2.resources.getColor(R.color.black)) // hex color 0xAARRGGBB
                                                textView2!!.text = list.get(j).comment
                                                a.addView(textView2)
                                                a.addView(image)


                                                ll_comments!!.addView(a);


                                            }




                                        }
                                    }
                                }

                            }
                            Log.e("stdferer", "" + listDataChild.size)
                            // listAdapter = ExpListViewAdapterWithCheckbox(this@PressPrint, listDataHeader, listDataChild)
                            // setting list adapter
                            //  expListView!!.setAdapter(listAdapter)

                        }
                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callsingleJobAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getSingleJob(assigned_id,job_department_id)
        Log.d("REQUEST", call.toString() + "-----" + assigned_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())

                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        listDataHeader = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<StartJobListDataResponse>? = response.body()!!.data!!
                        if (list!!.size > 0) {
                            for (j in 0 until list.size) {
                                listDataHeader.add(list.get(j).task_name.toString())
                                assigned_task_id = list.get(j).assigned_task_id.toString()
                                val top = ArrayList<StartJobListDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).toString())

                                listDataChild.put(listDataHeader[j], top)

                                ll_comments?.removeAllViews()
                                if (list.get(j).comments!!.size!! > 0) {
                                    val list: Array<CommentsListDataResponse>? = list.get(j).comments
                                    if (list!!.size > 0) {
                                        for (j in 0 until list.size) {
                                            Log.d("COMMENTSLIST", "" + list!!.size)


                                           /* if (list.get(j).user_type.equals("admin")) {
                                                val textViewadm = TextView(applicationContext)
                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.START
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                textViewadm.setPadding(2, 2, 2, 2)
                                                textViewadm!!.setLayoutParams(paramsadm)
                                                textViewadm!!.gravity = Gravity.LEFT
                                                textViewadm!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextColor(resources.getColor(R.color.tab2_bg))
                                                textViewadm!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textViewadm!!.setBackground(resources.getDrawable(R.drawable.bubble1))
                                                textViewadm!!.text = list.get(j).comment
                                                ll_comments?.addView(textViewadm)
                                                ll_comments!!.gravity = Gravity.LEFT
                                            }else if(list.get(j).user_type.equals("user")){
                                                val textView = TextView(applicationContext)
                                                // LinearLayout.LayoutParams  = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                params!!.gravity = Gravity.START
                                                params.setMargins(5, 5, 5, 5)
                                                textView.setPadding(2, 2, 2, 2)
                                                textView!!.setLayoutParams(params)
                                                textView!!.gravity = Gravity.LEFT
                                                textView!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                                                textView!!.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                                                textView!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textView!!.setBackground(resources.getDrawable(R.drawable.bubble1))
                                                textView!!.text = list.get(j).comment
                                                ll_comments?.addView(textView)
                                            }*/


                                            if (list.get(j).user_type.equals("admin")) {

                                                val textdrops = TextView(applicationContext)
                                                val image = ImageView(applicationContext)

                                                val ll_main = LinearLayout(applicationContext)
                                                val ll_name = LinearLayout(applicationContext)

                                                ll_name.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                                                ll_name.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_name.gravity = Gravity.LEFT
                                                ll_name.setPadding(10, 10, 10, 10)

                                                ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                                ll_main.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_main.gravity = Gravity.LEFT
                                                ll_main.setPadding(10, 10, 10, 10)


                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.LEFT
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                paramsadm.gravity = Gravity.LEFT
                                                textdrops!!.setLayoutParams(paramsadm)
                                                image!!.setLayoutParams(paramsadm)

                                                textdrops.setPadding(15, 15, 15, 15)
                                                textdrops!!.setLayoutParams(paramsadm)
                                                textdrops!!.gravity = Gravity.LEFT
                                                textdrops!!.setBackgroundColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textdrops!!.setBackground(resources.getDrawable(R.drawable.chat_bg))
                                                textdrops!!.text = list.get(j).comment


                                                image.layoutParams = android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                image.setImageDrawable(resources.getDrawable(R.drawable.admin_chat_icon))

                                                ll_main.addView(image)
                                                ll_name.addView(textdrops)

                                                ll_main.addView(ll_name)

                                               ll_comments?.addView(ll_main)

                                            } else if (list.get(j).user_type.equals("user")) {


                                                val textdrops = TextView(applicationContext)
                                                val image = ImageView(applicationContext)

                                                val ll_main = LinearLayout(applicationContext)
                                                val ll_name = LinearLayout(applicationContext)

                                                ll_name.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f));
                                                ll_name.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_name.gravity = Gravity.RIGHT
                                                ll_name.setPadding(10, 10, 10, 10)

                                                ll_main.setLayoutParams(LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                                ll_main.setOrientation(LinearLayout.HORIZONTAL)
                                                ll_main.gravity = Gravity.RIGHT
                                                ll_main.setPadding(10, 10, 10, 10)


                                                val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                paramsadm!!.gravity = Gravity.RIGHT
                                                paramsadm.setMargins(5, 5, 5, 5)
                                                paramsadm.gravity = Gravity.RIGHT
                                                textdrops!!.setLayoutParams(paramsadm)
                                                image!!.setLayoutParams(paramsadm)

                                                textdrops.setPadding(15, 15, 15, 15)
                                                textdrops!!.setLayoutParams(paramsadm)
                                                textdrops!!.gravity = Gravity.RIGHT
                                                textdrops!!.setBackgroundColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextColor(resources.getColor(R.color.black))
                                                textdrops!!.setTextSize(resources.getDimension(R.dimen.comment_text))
                                                textdrops!!.setBackground(resources.getDrawable(R.drawable.chat_bg_user))
                                                textdrops!!.text = list.get(j).comment


                                                image.layoutParams = android.view.ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                image.maxHeight = 15
                                                image.maxWidth = 15
                                                image.setImageDrawable(resources.getDrawable(R.drawable.user_chat_icon))

                                                ll_main.addView(image)
                                                ll_name.addView(textdrops)

                                                ll_main.addView(ll_name)

                                                ll_comments?.addView(ll_main)



                                            }


                                        }
                                    }
                                }
                                ll_sheets_count_id?.removeAllViews()
                                ll_s_no_id?.removeAllViews()
                                if(list.get(j).partiallyarr!!.size > 0){
                                    ll_sheets_list_id.visibility= View.VISIBLE
                                    val partiallyarrlist: Array<PartiallydonDataArray>? = list.get(j).partiallyarr
                                    if (partiallyarrlist!!.size > 0) {

                                        for (k in 0 until partiallyarrlist.size) {
                                            Log.d("COMMENTSLIST", "" + partiallyarrlist!!.size)
                                            val textViewsheets = TextView(applicationContext)
                                            val textViewCount = TextView(applicationContext)
                                            /*     val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                                 paramsadm!!.gravity = Gravity.START
                                                 paramsadm.setMargins(5, 5, 5, 5)
                                             textViewsheets.setPadding(2, 2, 2, 2)
                                             textViewsheets!!.setLayoutParams(paramsadm)*/
                                            textViewsheets!!.gravity = Gravity.LEFT
                                            textViewsheets!!.setText(partiallyarrlist.get(k).no_of_sheets.toString())
                                            textViewsheets!!.setTextColor(resources.getColor(R.color.cyan_color))
                                            //textViewsheets!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))
                                            //textViewsheets!!.setTextSize(resources.getDimension(R.dimen.text_size_de))
                                            textViewCount!!.setText((k+1).toString())
                                            textViewCount.setTextColor(resources.getColor(R.color.cyan_color))

                                            ll_sheets_count_id?.addView(textViewsheets)
                                            ll_s_no_id?.addView(textViewCount)


                                        }
                                    }
                                }else{
                                    ll_sheets_list_id.visibility=View.GONE
                                }
                            }
                            Log.e("stdferer", "" + listDataChild.size)


                        }

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callpostCommentsAPI(user_id :String, comments_text :String,  assigned_task_id :String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postTaskComment(assigned_task_id, user_id, comments_text)
        Log.d("POSTCOMMENTSAPI", user_id + "----" + comments_text + "-----" + assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())
                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result, Toast.LENGTH_SHORT).show();
                        et_comments.text.clear()
                          callsingleJobAPI()
                       // callstartJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


   /* private fun callpostDoneTaskAPI(assigned_id: String, total_sheets: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.updateSheets(assigned_id, total_sheets)
        Log.d("DONESHEETSAPI", assigned_id + "----" + total_sheets)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result, Toast.LENGTH_SHORT).show();
                        val home_intent = Intent(this@SheetsActivity, MainActivity::class.java)
                        startActivity(home_intent)
                        // callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }*/



    private fun callpostDoneTaskAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.doneTask("",assigned_task_id, "0", et_total_sheets!!.text.toString())
        Log.d("POSTDONEAPI", assigned_task_id+"---"+et_total_sheets!!.text.toString())
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")){
                        Log.w("Result_POSTCOMMENTS", "RESPONSESUCCESS")
                        Toast.makeText(getApplicationContext(),""+ response.body()!!.result,Toast.LENGTH_SHORT).show();
                        val home_intent = Intent(this@SheetsActivity2, MainActivity::class.java)
                        startActivity(home_intent)
                        callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    override fun onBackPressed() {
        val home_intent = Intent(this, DepartmentListActivity::class.java)
        startActivity(home_intent)
        finish()
        super.onBackPressed()

    }
}


