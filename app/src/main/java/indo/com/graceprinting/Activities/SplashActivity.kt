package indo.com.graceprinting.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.R

class SplashActivity : AppCompatActivity() {


    internal lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_splash)
        sessionManager = SessionManager(this)

        Handler().postDelayed({

            if (sessionManager.isLoggedIn) {
                val home_intent = Intent(this@SplashActivity, DepartmentListActivity::class.java)
                finish()
                startActivity(home_intent)
            }else{
                val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                this@SplashActivity.startActivity(mainIntent)
                this@SplashActivity.finish()
            }

        }, 2000)
    }
}