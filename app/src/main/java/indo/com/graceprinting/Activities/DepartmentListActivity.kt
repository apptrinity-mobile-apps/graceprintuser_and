package indo.com.graceprinting.Activities

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.Window
import android.widget.Toast
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprinting.Adapter.DepartmentListAdapter
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.Model.APIResponse.DepartmentListDataResponse
import indo.com.graceprinting.Model.APIResponse.DepartmentListResponse
import indo.com.graceprinting.R
import retrofit2.Call
import retrofit2.Callback


class DepartmentListActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var lv_userlist: RecyclerView? = null
    internal lateinit var my_loader: Dialog
    internal lateinit var user_id: String
    internal lateinit var user_name: String
    internal lateinit var dept_name: String
    private var mSearchListAdapter: DepartmentListAdapter? = null

    lateinit var drawer: DrawerLayout
    internal lateinit var sessionManager: SessionManager
    internal val PERMISSION_REQUEST_CODE = 111

    public var deptlistdata: ArrayList<DepartmentListDataResponse> = ArrayList<DepartmentListDataResponse>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deparment_menu)

        sessionManager = SessionManager(this)
        val user = sessionManager.userDetails

        user_name = user.get(SessionManager.PROFILE_USERNAME).toString()
        dept_name = user.get(SessionManager.DEPARTMENTNAME).toString()

        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close)

        drawer.setDrawerListener(toggle)
        toggle.syncState()

        toggle.setDrawerIndicatorEnabled(true);
        //toggle.setHomeAsUpIndicator(R.drawable.ic_launcher_background);

        toggle.setToolbarNavigationClickListener {

            drawer.openDrawer(GravityCompat.START);
        }

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        val header = navigationView.getHeaderView(0)
        val username = header.findViewById(R.id.tv_username) as CustomTextView
        val deptname = header.findViewById(R.id.tv_department_id) as CustomTextView
        username!!.setText(user_name)
        deptname!!.setText(dept_name)

       /* setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
*/
        /*toolbar.setNavigationOnClickListener {
          onBackPressed()}*/
        toolbar.setTitle("DEPARTMENTS")


        user_id = user.get(SessionManager.PROFILE_ID).toString()
        myloading()


        lv_userlist = findViewById(R.id.lv_depmentlist) as RecyclerView
        callSearchList()
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission()
            } else {

            }
        } else {

        }
    }

    private fun checkReadExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkWriteExternalStoragePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
    }
    private fun callSearchList() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getDepartmentwiseCount(user_id)
        Log.d("REQUEST", call.toString() + "------"+user_id)
        call.enqueue(object : Callback<DepartmentListResponse> {
            override fun onResponse(call: Call<DepartmentListResponse>, response: retrofit2.Response<DepartmentListResponse>?) {
                my_loader.dismiss()
                Log.w("Status", "*** " + response!!.body()!!.status)
                if (response != null && response.body()!!.status.equals("1")) {
                    if (response.body()!!.data != null) {
                        my_loader.dismiss()
                        val list: Array<DepartmentListDataResponse> = response.body()!!.data!!

                        for (item: DepartmentListDataResponse in list.iterator()) {
                            deptlistdata.add(item)

                            //storeDepartmentData(item)
                            setProductAdapter(deptlistdata)


                            lv_userlist!!.setHasFixedSize(true)
                            val gridLayoutManager = GridLayoutManager(this@DepartmentListActivity, 2)
                            lv_userlist!!.setLayoutManager(gridLayoutManager)
                            lv_userlist!!.setItemAnimator(DefaultItemAnimator())
                            val details_adapter = DepartmentListAdapter(deptlistdata, this@DepartmentListActivity!!)
                            lv_userlist!!.setAdapter(details_adapter)
                            details_adapter.notifyDataSetChanged()


                        }
                        if (deptlistdata.size == 0) {

                        }
                    }

                }else {
                    Toast.makeText(applicationContext,"Failed to Respond Data",Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<DepartmentListResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun setProductAdapter(deptlistdata: ArrayList<DepartmentListDataResponse>) {
        mSearchListAdapter = DepartmentListAdapter(deptlistdata, this)
        lv_userlist!!.adapter = mSearchListAdapter
        mSearchListAdapter!!.notifyDataSetChanged()
    }


   /* private fun storeDepartmentData(user_info: DepartmentListDataResponse?) {
        if (user_info != null) {
            try {
                sessionManager.createDepartmentSession(user_info.department_id!!)
                *//*if (user_info.id != null && user_info.department_id != null && user_info.department_name != null && user_info.user_name != null && user_info.user_email != null && user_info.user_mobile_number != null) {

                }*//*
            } catch (e: Exception) {
                Log.w("exception", "" + e.printStackTrace())
            }

        }

    }*/



    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            val a = Intent(Intent.ACTION_MAIN)
            a.addCategory(Intent.CATEGORY_HOME)
            a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(a)
            super.onBackPressed()
        }
    }

   /* override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }*/

   /* override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> false
            else -> super.onOptionsItemSelected(item)
        }
    }*/

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                val intent = Intent(this,DepartmentListActivity::class.java)
                startActivity(intent)
            }
         /*R.id.nav_main ->{
             val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }*/
            R.id.nav_logout -> {
                sessionManager.logoutUser()
            }

        }

        drawer.closeDrawer(GravityCompat.START)

        return true
    }

    // Extension function to show toast message easily
    private fun Context.toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }


}
