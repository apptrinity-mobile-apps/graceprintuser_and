package indo.com.graceprinting.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.*
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import com.squareup.picasso.Picasso
import indo.com.graceprinting.Helper.ConnectionManager
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.LocationUtil.PermissionUtils
import indo.com.graceprinting.LocationUtil.PermissionUtils.PermissionResultCallback
import indo.com.graceprinting.Model.APIResponse.*
import indo.com.graceprinting.R
import kotlinx.android.synthetic.main.task_list_item_jobs.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PressPrint : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnRequestPermissionsResultCallback,
        PermissionResultCallback {


    private val PLAY_SERVICES_REQUEST = 1000
    private val REQUEST_CHECK_SETTINGS = 2000


    // Google client to interact with Google API

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null

    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()

    // list of permissions

    internal var permissions = java.util.ArrayList<String>()
    internal lateinit var permissionUtils: PermissionUtils

    internal var isPermissionGranted: Boolean = false

    var final_gps_location_stg = ""

    internal var delivery_done_status: Boolean = false


    private var expListView: ExpandableListView? = null
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataChild: HashMap<String, ArrayList<StartJobListDataResponse>>
    lateinit var listDataChild1: HashMap<String, List<String>>
    private var lastExpandedPosition = -1
    var job_id: String? = null
    lateinit var assigned_id: String
    var job_department_id: String = ""
    lateinit var task_name: String
    var task_name_notification: String = ""
    lateinit var active_continue: String
    lateinit var department_id: String
    internal lateinit var sessionManager: SessionManager
    internal lateinit var user_id: String
    internal lateinit var assigned_task_id: String
    internal lateinit var comments_text: String
    internal lateinit var my_loader: Dialog

    internal lateinit var mClear: Button
    internal lateinit var mGetSign: Button
    internal lateinit var mCancel: Button

    internal lateinit var file: File
    internal lateinit var dialog: Dialog
    internal lateinit var mContent: LinearLayout
    internal lateinit var view: View
    internal lateinit var mSignature: signature
    internal var bitmap: Bitmap? = null
    internal var str: String? = null
    internal var depart_type_name: String? = null

    internal lateinit var dialog_delivery_partial: Dialog

    private var mIsDestroyed: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_press_print)


        val toolbar = findViewById(R.id.toolbar) as Toolbar
        val tv_job_id_value = findViewById(R.id.tv_job_id_value) as CustomTextView
        val rootview = findViewById(R.id.ll_login_id) as LinearLayout

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationOnClickListener {
            val intent = Intent(this, DepartmentListActivity::class.java)
            startActivity(intent)

        }
        toolbar.setTitle("GRACE PRINTING")
        myloading()



        sessionManager = SessionManager(this)
        sessionManager.checkLogin()
        val user = sessionManager.userDetails

        user_id = user.get(SessionManager.PROFILE_ID).toString()
        Log.e("user_id", user_id);

        job_id = intent.getStringExtra("job_id")
        assigned_id = intent.getStringExtra("assigned_id")
        if (intent.getStringExtra("job_department_id") != null) {
            job_department_id = intent.getStringExtra("job_department_id")
        }
        if (intent.getStringExtra("department_id") != null) {
            department_id = intent.getStringExtra("department_id")
        }


        if (intent.getStringExtra("task_name") != null) {
            task_name_notification = intent.getStringExtra("task_name")
        }

        Log.e("NOTIFY_LLL", job_id + "-----" + assigned_id + "---" + department_id + "-----" + job_department_id);

        active_continue = intent.getStringExtra("active_continue")

        if (active_continue.equals("continue")) {
            if (ConnectionManager.checkConnection(applicationContext)) {
                callsingleJobAPI()
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, applicationContext)
            }


        } else if (active_continue.equals("active")) {

            if (ConnectionManager.checkConnection(applicationContext)) {
                callstartJobAPI()
            } else {
                ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, applicationContext)
            }


        }

        tv_job_id_value.setText("Job - " + job_id)


        expListView = findViewById(R.id.lvExp) as ExpandableListView


        // Listview Group click listener
        expListView!!.setOnGroupClickListener { parent, v, groupPosition, id ->
            // Toast.makeText(getApplicationContext(),
            // "Group Clicked " + listDataHeader.get(groupPosition),
            // Toast.LENGTH_SHORT).show();

            false
        }

        // Listview Group expanded listener
        expListView!!.setOnGroupExpandListener { groupPosition ->
            /* Toast.makeText(this@PressPrint,
                     listDataHeader[groupPosition] + " Expanded",
                     Toast.LENGTH_SHORT).show()*/


            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                expListView!!.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;
        }

        // Listview Group collasped listener
        expListView!!.setOnGroupCollapseListener { groupPosition ->
            /*Toast.makeText(this@PressPrint,
                    listDataHeader[groupPosition] + " Collapsed",
                    Toast.LENGTH_SHORT).show()*/
        }

        // Listview on child click listener
        expListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            // TODO Auto-generated method stub
            /*Toast.makeText(
                    this@PressPrint,
                    listDataHeader[groupPosition]
                            + " : "
                            + listDataChild[listDataHeader[groupPosition]]!!.get(
                            childPosition), Toast.LENGTH_SHORT)
                    .show()*/
            false
        }

        // Dialog Function
        dialog = Dialog(this@PressPrint)
        // Removing the features of Normal Dialogs
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_signature)
        dialog.setCancelable(true)



        permissionUtils = PermissionUtils(this@PressPrint)

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)

        permissionUtils.check_permission(permissions, "Need GPS permission for getting your location", 1)
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient()
        }
    }


    private fun callstartJobAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.startJob(assigned_id, job_department_id)
        Log.d("REQUEST", call.toString() + "-----" + assigned_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details press", response.body().toString())
                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        listDataHeader = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<StartJobListDataResponse>? = response.body()!!.data!!
                        if (list!!.size > 0) {
                            for (j in 0 until list.size) {

                                listDataHeader.add(list.get(j).task_name.toString())
                                //listDataHeader.add(list.get(j).assigned_task_id.toString())

                                val top = ArrayList<StartJobListDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details press", list.get(j).toString())

                                listDataChild.put(listDataHeader[j], top)
                            }
                            Log.e("stdferer press", "" + listDataChild.size)

                            listAdapter = ExpListViewAdapterWithCheckbox(this@PressPrint, listDataHeader, listDataChild)
                            // setting list adapter
                            expListView!!.setAdapter(listAdapter)
                            expListView!!.expandGroup(0)

                        }
                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callsingleJobAPI() {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.getSingleJob(assigned_id, job_department_id)
        Log.d("REQUESTsingleJob", call.toString() + "-----" + assigned_id + "----" + job_department_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())

                    if (response.body()!!.status.equals("1") && response.body()!!.data != null) {
                        listDataHeader = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<StartJobListDataResponse>? = response.body()!!.data!!
                        if (list!!.size > 0) {

                            delivery_done_status == true
                            for (j in 0 until list.size) {
                                listDataHeader.add(list.get(j).task_name.toString())
                                //listDataHeader.add(list.get(j).assigned_task_id.toString())
                                val top = ArrayList<StartJobListDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).toString())
                                listDataChild.put(listDataHeader[j], top)


                                if (list.get(j).partiallyarr!!.size > 0) {
                                    val partiallyarrlist: Array<PartiallydonDataArray>? = list.get(j).partiallyarr

                                }


                            }


                            Log.e("stdferer", "" + listDataChild.size)

                            listAdapter = ExpListViewAdapterWithCheckbox(this@PressPrint, listDataHeader, listDataChild)
                            // setting list adapter
                            expListView!!.setAdapter(listAdapter)

                            expListView!!.expandGroup(0)
                            if (task_name_notification != "" && listDataHeader.size > 0) {
                                val sdasd = listDataHeader!!.indexOf(task_name_notification)
                                expListView!!.expandGroup(sdasd)
                            }

                        }

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_FAILED", t.toString())
            }
        })
    }


    private fun callpostCommentsAPI(user_id: String, comments_text: String, assigned_task_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.postTaskComment(assigned_task_id, user_id, comments_text)
        Log.d("POSTCOMMENTSAPI", user_id + "----" + comments_text + "-----" + assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result, Toast.LENGTH_SHORT).show();
                        et_comments.text.clear()
                        callsingleJobAPI()
                        //callstartJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callpostDoneTaskAPI(image_name: String, assigned_task_id: String, next_assigned_task_id: String, par_status: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.doneTaskBindery(image_name, assigned_task_id, next_assigned_task_id, "", par_status)
        Log.d("POSTCOMMENTSAPI", assigned_task_id + "---" + next_assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result + "----" + response.body()!!.next_assigned_task_id, Toast.LENGTH_SHORT).show();
                        callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun calldoneTaskMailintoDeliveryAPI(image_name: String, assigned_task_id: String, next_assigned_task_id: String, type: String, status: String, delivery_status: String, delivery_date: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.doneTaskMailintoDelivery(image_name, assigned_task_id, next_assigned_task_id, type, status, delivery_status, delivery_date)
        Log.d("POSTCOMMENTSAPI", assigned_task_id + "---" + next_assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result + "----" + response.body()!!.next_assigned_task_id, Toast.LENGTH_SHORT).show();
                        callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun callDoneDelivertAPI(image_name: File?, assigned_task_id: String, next_assigned_task_id: String, type: String, status: String, delivery_location: String, delivery_date: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), assigned_task_id)
        val next_assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), next_assigned_task_id)
        val type_val = RequestBody.create(MediaType.parse("text/plain"), type)
        val status_val = RequestBody.create(MediaType.parse("text/plain"), status)
        val delivery_location_val = RequestBody.create(MediaType.parse("text/plain"), delivery_location)
        val delivery_date_val = RequestBody.create(MediaType.parse("text/plain"), delivery_date)

        val requestBody = RequestBody.create(MediaType.parse("*/*"), image_name)
        val fileToUpload = MultipartBody.Part.createFormData("delivery_sign", image_name!!.getName(), requestBody)

        val call = apiService.doneTaskDelivery(fileToUpload, assigned_task_id_val, next_assigned_task_id_val, type_val, status_val, delivery_location_val, delivery_date_val)
        Log.d("POSTCOMMENTSAPI_LLLL", assigned_task_id + "---" + next_assigned_task_id + type_val + "---" + status_val + "---" + delivery_location_val + "---" + delivery_date_val)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result + "----" + response.body()!!.next_assigned_task_id, Toast.LENGTH_SHORT).show();
                        callsingleJobAPI()


                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun callpostDoneTaskAPI(image_name: String, assigned_task_id: String, next_assigned_task_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val call = apiService.doneTask(image_name, assigned_task_id, next_assigned_task_id, "")
        Log.d("POSTCOMMENTSAPI", assigned_task_id + "---" + next_assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result + "----" + response.body()!!.next_assigned_task_id, Toast.LENGTH_SHORT).show();
                        callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun callpartiallyDonesheetsAPI(image_name: String, assigned_task_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.partiallyDonesheetsAPI(image_name, assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {

                        callsingleJobAPI()
                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun callpartiallyDoneDeliverysAPIAPI(type: String, drop_status: String, id: String, ass_task_id: String, nxt_ass_task_id: String, location: String, date: String, file: File) {
        my_loader.show()
        val apiService = ApiInterface.create()


        val assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), ass_task_id)
        val next_assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), nxt_ass_task_id)
        val type_val = RequestBody.create(MediaType.parse("text/plain"), type)
        val drop_status_val = RequestBody.create(MediaType.parse("text/plain"), drop_status)
        val id_val = RequestBody.create(MediaType.parse("text/plain"), id)
        val location_val = RequestBody.create(MediaType.parse("text/plain"), location)
        val date_val = RequestBody.create(MediaType.parse("text/plain"), date)

        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("delivery_sign", file!!.getName(), requestBody)

        val call = apiService.partiallyDoneDeliverysAPI(fileToUpload, type_val, drop_status_val, id_val, assigned_task_id_val, next_assigned_task_id_val, location_val, date_val)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {

                        callsingleJobAPI()
                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun callpartiallyDonedropsAPI(dep_type: String, dep_status: String, id: String, assigned_task_id: String, next_assigned_task_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.partiallyDonedropsAPI(dep_type, dep_status, id, assigned_task_id, next_assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {

                        callsingleJobAPI()
                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }

    private fun callpostDoneTaskAPI1(image_name: File?, assigned_task_id: String, next_assigned_task_id: String) {
        my_loader.show()
        val apiService = ApiInterface.create()

        val assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), assigned_task_id)
        val next_assigned_task_id_val = RequestBody.create(MediaType.parse("text/plain"), next_assigned_task_id)
        val usedsheets_val = RequestBody.create(MediaType.parse("text/plain"), "")
        val requestBody = RequestBody.create(MediaType.parse("*/*"), image_name)
        val fileToUpload = MultipartBody.Part.createFormData("delivery_sign", image_name!!.getName(), requestBody)


        val call = apiService.doneTask1(fileToUpload, assigned_task_id_val, next_assigned_task_id_val, usedsheets_val, "", "")
        Log.d("POSTCOMMENTSAPI", assigned_task_id + "---" + next_assigned_task_id)
        call.enqueue(object : Callback<StartJobResponse> {
            override fun onResponse(call: Call<StartJobResponse>, response: retrofit2.Response<StartJobResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_POSTCOMMENTS", response.body().toString())

                    if (response.body()!!.status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "" + response.body()!!.result + "----" + response.body()!!.next_assigned_task_id, Toast.LENGTH_SHORT).show();
                        callsingleJobAPI()

                    }

                }
            }

            override fun onFailure(call: Call<StartJobResponse>, t: Throwable) {
                Log.w("Result_Order_details", t.toString())
            }
        })
    }


    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    inner class ExpListViewAdapterWithCheckbox/*  Here's the constructor we'll use to pass in our calling
         *  activity's context, group items, and child items
        */
    (// Define activity context
            private val mContext: Context, // ArrayList that is what each key in the above
            // hashmap points to
            private val mListDataGroup: ArrayList<String>, /*
         * Here we have a Hashmap containing a String key
         * (can be Integer or other type but I was testing
         * with contacts so I used contact name as the key)
        */
            private val mListDataChild: HashMap<String, ArrayList<StartJobListDataResponse>>) : BaseExpandableListAdapter() {

        // Hashmap for keeping track of our checkbox check states
        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        // Our getChildView & getGroupView use the viewholder patter
        // Here are the viewholders defined, the inner classes are
        // at the bottom
        private var childViewHolder: ChildViewHolder? = null
        private var groupViewHolder: GroupViewHolder? = null

        /*
              *  For the purpose of this document, I'm only using a single
         *	textview in the group (parent) and child, but you're limited only
         *	by your XML view for each group item :)
        */
        private var groupText: String? = null
        private var childText: String? = null

        init {

            // Initialize our hashmap containing our check states here
            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }

        /*
         * This defaults to "public object getGroup" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListGroupItems".
            //  Here is where I call the getter to get that text
            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.detail_listgroup, null)

                // Initialize the GroupViewHolder defined at the bottom of this document
                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.mGroupText = convertView!!.findViewById(R.id.lblListHeader) as TextView
                groupViewHolder!!.arrow_imageView = convertView!!.findViewById(R.id.lblListHeader_image) as ImageView


                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            groupViewHolder!!.mGroupText!!.text = groupText



            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        /*
         * This defaults to "public object getChild" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getChild(groupPosition: Int, childPosition: Int): StartJobListDataResponse {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        @SuppressLint("NewApi")
        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            val mGroupPosition = groupPosition

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListChildItems".
            //  Here is where I call the getter to get that text
            // childText = getChild(groupPosition, childPosition)

            if (convertView == null) {

                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.task_list_item_jobs, null)

                childViewHolder = ChildViewHolder()
                childViewHolder!!.tv_comments = convertView!!.findViewById(R.id.tv_comments)
                childViewHolder!!.ll_comments = convertView!!.findViewById(R.id.ll_comments)
                childViewHolder!!.et_comments = convertView!!.findViewById(R.id.et_comments)
                childViewHolder!!.btn_postcomment = convertView!!.findViewById(R.id.btn_postcomment)
                childViewHolder!!.btn_done = convertView!!.findViewById(R.id.btn_done)
                childViewHolder!!.btn_par_done = convertView!!.findViewById(R.id.btn_par_done)

                childViewHolder!!.ll_btn_cmt_done = convertView!!.findViewById(R.id.ll_btn_cmt_done)
                childViewHolder!!.no_service = convertView!!.findViewById(R.id.no_service)


                childViewHolder!!.ll_s_no_id = convertView!!.findViewById(R.id.ll_s_no_id)
                childViewHolder!!.ll_sheets_count_id = convertView!!.findViewById(R.id.ll_sheets_count_id)
                childViewHolder!!.ll_partly_done_id = convertView!!.findViewById(R.id.ll_partly_done_id)
                childViewHolder!!.ll_sheets_list_id = convertView!!.findViewById(R.id.ll_sheets_list_id)

                childViewHolder!!.ll_drops_list_id = convertView!!.findViewById(R.id.ll_drops_list_id)
                childViewHolder!!.ll_drop_sno_id = convertView!!.findViewById(R.id.ll_drop_sno_id)
                childViewHolder!!.ll_no_drop_id = convertView!!.findViewById(R.id.ll_no_drop_id)
                childViewHolder!!.ll_drop_date_id = convertView!!.findViewById(R.id.ll_drop_date_id)
                childViewHolder!!.ll_drops_done_id = convertView!!.findViewById(R.id.ll_drops_done_id)



                childViewHolder!!.ll_drops_id = convertView!!.findViewById(R.id.ll_drops_id)
                childViewHolder!!.ll_drops_main_id = convertView!!.findViewById(R.id.ll_drops_main_id)
                childViewHolder!!.ll_delivery_view_id = convertView!!.findViewById(R.id.ll_delivery_view_id)


                convertView.setTag(R.layout.task_list_item_jobs, childViewHolder)

            } else {
                childViewHolder = convertView
                        .getTag(R.layout.task_list_item_jobs) as ChildViewHolder
            }




            if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_status.equals("1")) {
                childViewHolder!!.ll_btn_cmt_done!!.visibility = View.VISIBLE
                childViewHolder!!.et_comments!!.visibility = View.VISIBLE
            } else if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_status.equals("2")) {
                childViewHolder!!.ll_btn_cmt_done!!.visibility = View.GONE
                childViewHolder!!.et_comments!!.visibility = View.GONE
            } else {
                childViewHolder!!.ll_btn_cmt_done!!.visibility = View.VISIBLE
                childViewHolder!!.et_comments!!.visibility = View.VISIBLE
            }

            Log.d("COMMENTSLIST_SIZE", "" + listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).comments!!.size)
            childViewHolder!!.ll_comments?.removeAllViews()

            childViewHolder!!.ll_sheets_count_id?.removeAllViews()
            childViewHolder!!.ll_s_no_id?.removeAllViews()
            childViewHolder!!.ll_partly_done_id?.removeAllViews()


            childViewHolder!!.ll_drop_sno_id?.removeAllViews()
            childViewHolder!!.ll_no_drop_id?.removeAllViews()
            childViewHolder!!.ll_drop_date_id?.removeAllViews()
            childViewHolder!!.ll_drops_done_id?.removeAllViews()

            if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).comments!!.size!! > 0) {
                childViewHolder!!.no_service!!.visibility = View.GONE
                val list: Array<CommentsListDataResponse>? = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).comments
                if (list!!.size > 0) {
                    for (j in 0 until list.size) {
                        Log.d("COMMENTSLIST", "" + list!!.size + "------" + listDataHeader[groupPosition] + "" + list.get(j).comment)

                        if (list.get(j).user_type.equals("admin")) {

                            val a = LinearLayout(mContext);
                            a.setOrientation(LinearLayout.HORIZONTAL);
                            a.setLayoutParams(LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            a.setPadding(10, 10, 10, 10)
                            val textView1 = TextView(mContext);

                            val image = ImageView(mContext)
                            image.setImageDrawable(mContext.resources.getDrawable(R.drawable.admin_chat_icon));
                            image.maxHeight = 15
                            image.maxWidth = 15
                            textView1!!.setBackground(mContext.resources.getDrawable(R.drawable.chat_bg))
                            textView1!!.setTextColor(mContext.resources.getColor(R.color.black)) // hex color 0xAARRGGBB
                            textView1!!.text = list.get(j).comment
                            a.addView(image)
                            a.addView(textView1)

                            childViewHolder!!.ll_comments!!.addView(a);

                        } else if (list.get(j).user_type.equals("user")) {

                            val a = LinearLayout(mContext);
                            a.setOrientation(LinearLayout.HORIZONTAL);
                            val textView2 = TextView(mContext);
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)


                            layoutParams.gravity = Gravity.RIGHT;
                            a.setPadding(10, 10, 10, 10); // (left, top, right, bottom)
                            a.setLayoutParams(layoutParams);
                            val image = ImageView(mContext)
                            image.setImageDrawable(mContext.resources.getDrawable(R.drawable.user_chat_icon));
                            image.maxHeight = 15
                            image.maxWidth = 15
                            textView2!!.setBackground(mContext.resources.getDrawable(R.drawable.chat_bg))
                            textView2!!.setTextColor(mContext.resources.getColor(R.color.black)) // hex color 0xAARRGGBB
                            textView2!!.text = list.get(j).comment
                            a.addView(textView2)
                            a.addView(image)


                            childViewHolder!!.ll_comments!!.addView(a);


                        }


                    }
                }


            } else if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).comments!!.size!! == 0) {
                Log.d("COMMENTSLISTSIZE", "" + listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).comments!!.size!!)
                // childViewHolder!!.no_service!!.text = "No Comments Found"
                childViewHolder!!.no_service!!.visibility = View.GONE
            }

            val partiallyarrlist: Array<PartiallydonDataArray>? = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).partiallyarr
            if (department_id.equals("3")) {
                childViewHolder!!.ll_sheets_list_id!!.visibility = View.VISIBLE
            } else {
                childViewHolder!!.ll_sheets_list_id!!.visibility = View.GONE
            }




            if (partiallyarrlist!!.size > 0) {

                for (k in 0 until partiallyarrlist!!.size) {
                    Log.d("COMMENTSLIST", "" + partiallyarrlist!!.size)
                    val textViewsheets = TextView(applicationContext)
                    val textViewCount = TextView(applicationContext)
                    val textViewpartlyDone = TextView(applicationContext)

                    val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    //paramsadm!!.gravity = Gravity.RIGHT
                    paramsadm.setMargins(0, 0, 0, 0)
                    textViewpartlyDone.setPadding(0, 0, 0, 0)

                    textViewCount!!.setLayoutParams(paramsadm)
                    textViewpartlyDone!!.setLayoutParams(paramsadm)
                    textViewsheets!!.setLayoutParams(paramsadm)

                    textViewsheets!!.gravity = Gravity.LEFT
                    textViewsheets!!.setText(partiallyarrlist.get(k).no_of_sheets.toString())
                    textViewsheets!!.setTextColor(resources.getColor(R.color.cyan_color))
                    textViewsheets!!.setPadding(2, 5, 2, 5)
                    //textViewsheets!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))
                    //textViewsheets!!.setTextSize(resources.getDimension(R.dimen.text_size_de))
                    textViewCount!!.setText((k + 1).toString())
                    textViewCount.setTextColor(resources.getColor(R.color.cyan_color))
                    textViewCount!!.setPadding(2, 5, 2, 5)

                    textViewpartlyDone.gravity = Gravity.RIGHT



                    if (mGroupPosition.equals(partiallyarrlist.get(k).status!!.toInt())) {
                        textViewpartlyDone.setText("Partially Done")
                        textViewpartlyDone.setSingleLine(false)
                        textViewpartlyDone.setEllipsize(TextUtils.TruncateAt.END)
                        val n = 1 // the exact number of lines you want to display
                        textViewpartlyDone.setLines(n)
                        textViewpartlyDone!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))
                        textViewpartlyDone.setOnClickListener {
                            Log.d("done_position", "" + k)
                            callpartiallyDonesheetsAPI(partiallyarrlist.get(k).par_id.toString(), (mGroupPosition + 1).toString())
                        }
                    } else if (mGroupPosition < (partiallyarrlist.get(k).status!!.toInt())) {
                        textViewpartlyDone.setText("Done")
                        textViewpartlyDone!!.setBackgroundColor(resources.getColor(R.color.tab4_bg))
                    } else if (mGroupPosition > (partiallyarrlist.get(k).status!!.toInt())) {
                        textViewpartlyDone.setText("Not Started")
                        textViewpartlyDone!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                    }

                    textViewpartlyDone.setAllCaps(false)

                    textViewpartlyDone!!.setTextColor(resources.getColor(R.color.white))
                    childViewHolder!!.ll_sheets_count_id?.addView(textViewsheets)
                    childViewHolder!!.ll_s_no_id?.addView(textViewCount)
                    childViewHolder!!.ll_partly_done_id?.addView(textViewpartlyDone)


                }
            } else {
                childViewHolder!!.ll_sheets_list_id?.visibility = View.GONE
            }


            ///mailing
            val dropsarrList: Array<Dropsarraylist>? = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).dropsarr
            if (department_id.equals("4")) {
                childViewHolder!!.ll_drops_list_id!!.visibility = View.VISIBLE
                depart_type_name = "mailing"
            } else if (department_id.equals("5")) {
                childViewHolder!!.ll_drops_list_id!!.visibility = View.VISIBLE
                depart_type_name = "shipping"
            } else if (department_id.equals("8")) {
                childViewHolder!!.ll_drops_list_id!!.visibility = View.VISIBLE
                depart_type_name = "delivery"

            } else {
                childViewHolder!!.ll_drops_list_id!!.visibility = View.GONE
            }

            if (dropsarrList!!.size > 0) {

                Log.e("DROPARRAYLISTSIZE", dropsarrList!!.size.toString())

                for (m in 0 until dropsarrList!!.size) {
                    val textdrops_sno = TextView(applicationContext)
                    val textdrops = TextView(applicationContext)
                    val textdropsdate = TextView(applicationContext)
                    val textDropsstatus = TextView(applicationContext)

                    val paramsadm = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    //paramsadm!!.gravity = Gravity.RIGHT
                    paramsadm.setMargins(2, 2, 2, 2)
                    textDropsstatus.setPadding(2, 2, 2, 2)
                    textdrops_sno!!.setLayoutParams(paramsadm)
                    textdrops!!.setLayoutParams(paramsadm)
                    textDropsstatus!!.setLayoutParams(paramsadm)
                    textdropsdate!!.setLayoutParams(paramsadm)

                    textdrops!!.setText(dropsarrList.get(m).term.toString())
                    textdrops!!.setTextColor(resources.getColor(R.color.cyan_color))
                    textdrops!!.setPadding(2, 2, 2, 2)


                    val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                    val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                    val inputDateStr = dropsarrList.get(m).date.toString()
                    val date = inputFormat.parse(inputDateStr)
                    val outputDateStr = outputFormat.format(date)
                    Log.e("DROPDATE", dropsarrList.get(m).date.toString() + "------" + outputDateStr)

                    textdropsdate!!.setText(outputDateStr)
                    textdropsdate!!.setTextColor(resources.getColor(R.color.cyan_color))
                    textdropsdate!!.setPadding(2, 2, 2, 2)

                    textdrops_sno!!.setText((m + 1).toString())
                    textdrops_sno.setTextColor(resources.getColor(R.color.cyan_color))
                    textdrops_sno!!.setPadding(2, 2, 2, 2)


                    /*  val size_list = mListDataChild.size - mGroupPosition
                      if (size_list == 1) {*/
                    assigned_task_id = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id!!

                    /* } else {
                         assigned_task_id = listDataChild[listDataHeader[groupPosition + 1]]!!.get(childPosition).assigned_task_id!!

                     }*/


                    if (department_id.equals("4")) {
                        if (mGroupPosition.equals(dropsarrList.get(m).mailing_status!!.toInt())) {
                            textDropsstatus.setText("Partially Done")
                            textDropsstatus.setSingleLine(false)
                            textDropsstatus.setEllipsize(TextUtils.TruncateAt.END)
                            val n = 1 // the exact number of lines you want to display
                            textDropsstatus.setLines(n)
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))
                            textDropsstatus.setOnClickListener {
                                Log.d("done_position", "" + m)

                                val s = mListDataChild.size - mGroupPosition
                                val next_assigned_task_id: String
                                if (s == 1) {
                                    next_assigned_task_id = "0"
                                } else {
                                    next_assigned_task_id = assigned_task_id
                                }
                                callpartiallyDonedropsAPI(depart_type_name!!, (mGroupPosition + 1).toString(), dropsarrList.get(m).id.toString(), assigned_task_id, next_assigned_task_id)
                            }
                        } else if (mGroupPosition < (dropsarrList.get(m).mailing_status!!.toInt())) {
                            textDropsstatus.setText("Done")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab4_bg))
                        } else if (mGroupPosition > (dropsarrList.get(m).mailing_status!!.toInt())) {
                            textDropsstatus.setText("Not Started")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                        }
                    } else if (department_id.equals("5")) {
                        ///shipping
                        if (mGroupPosition.equals(dropsarrList.get(m).shipping_status!!.toInt())) {
                            textDropsstatus.setText("Partially Done")
                            textDropsstatus.setSingleLine(false)
                            textDropsstatus.setEllipsize(TextUtils.TruncateAt.END)
                            val n = 1 // the exact number of lines you want to display
                            textDropsstatus.setLines(n)
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))
                            textDropsstatus.setOnClickListener {
                                Log.d("done_position", "" + m)

                                val s = mListDataChild.size - mGroupPosition
                                val next_assigned_task_id: String
                                if (s == 1) {
                                    next_assigned_task_id = "0"
                                } else {
                                    next_assigned_task_id = assigned_task_id
                                }


                                callpartiallyDonedropsAPI(depart_type_name!!, (mGroupPosition + 1).toString(), dropsarrList.get(m).id.toString(), assigned_task_id, next_assigned_task_id)
                            }
                        } else if (mGroupPosition < (dropsarrList.get(m).shipping_status!!.toInt())) {
                            textDropsstatus.setText("Done")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab4_bg))
                        } else if (mGroupPosition > (dropsarrList.get(m).shipping_status!!.toInt())) {
                            textDropsstatus.setText("Not Started")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                        }

                    } else if (department_id.equals("8")) {
                        ///Delivery
                        getLocation()

                        if (mLastLocation != null) run {
                            latitude = mLastLocation!!.getLatitude()
                            longitude = mLastLocation!!.getLongitude()
                            getAddress()
                        }

                        if (mGroupPosition.equals(dropsarrList.get(m).delivery_status!!.toInt())) {
                            textDropsstatus.setText("Partially Done")
                            textDropsstatus.maxLines = 1
                            textDropsstatus.setSingleLine(false)
                            textDropsstatus.setEllipsize(TextUtils.TruncateAt.END)
                            val n = 1 // the exact number of lines you want to display
                            textDropsstatus.setLines(n)
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab3_bg))

                            Log.d("LASTDROPDONESTATUS", "" + m + "------" + dropsarrList.get(m).last_done_status)
                            //TODO
                            //Log.d("DROPSIZE", "" + m+"DELIVERYSTATUS"+dropsarrList.get(m)-1)

                            if (dropsarrList.get(m).last_done_status.equals("1")) {
                                if (mGroupPosition.equals(dropsarrList.get(m).delivery_status!!.toInt())) {
                                    Log.d("LASTDROPDONESTATUS_IF", "" + m + "------" + dropsarrList.get(m).last_done_status)
                                    textDropsstatus.setText("Done")
                                    textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.colorDarkpink))

                                }
                            }


                            textDropsstatus.setOnClickListener {

                                if (dropsarrList.get(m).last_done_status.equals("1")) {
                                    Log.d("PARTIALDONEIF", dropsarrList.get(m).last_done_status)

                                    val s = mListDataChild.size - mGroupPosition
                                    val next_assigned_task_id: String
                                    if (s == 1) {
                                        next_assigned_task_id = "0"
                                    } else {
                                        next_assigned_task_id = assigned_task_id
                                    }

                                    depart_type_name = "delivery"
                                    childViewHolder!!.btn_done!!.visibility = View.GONE

                                    getLocation()

                                    if (mLastLocation != null) run {
                                        latitude = mLastLocation!!.getLatitude()
                                        longitude = mLastLocation!!.getLongitude()
                                        getAddress()

                                    }

                                    dialog_action(assigned_task_id, next_assigned_task_id, (mGroupPosition + 1).toString(), "", "")


                                } else {

                                    // Log.d("PARTIALDONEELSE", dropsarrList.get(m).last_done_status)

                                    val s = mListDataChild.size - mGroupPosition
                                    val next_assigned_task_id: String
                                    if (s == 1) {
                                        next_assigned_task_id = "0"
                                    } else {
                                        next_assigned_task_id = assigned_task_id
                                    }

                                    dialog_action(assigned_task_id, next_assigned_task_id, (mGroupPosition + 1).toString(), "partially", dropsarrList.get(m).id.toString())

                                }


                                // callpartiallyDonedropsAPI(depart_type_name!!,(mGroupPosition+1).toString(),dropsarrList.get(m).id.toString(),assigned_task_id,next_assigned_task_id)

                            }
                        } else if (mGroupPosition < (dropsarrList.get(m).delivery_status!!.toInt())) {

                            textDropsstatus.setText("View")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab4_bg))

                            textDropsstatus!!.setOnClickListener {
                               // dialog_delivery_view(dropsarrList.get(m).term.toString(), dropsarrList.get(m).date.toString(), dropsarrList.get(m).delivery_date.toString(), dropsarrList.get(m).delivery_location.toString(), dropsarrList.get(m).delivery_sign.toString())


                                dialog_delivery_partial = Dialog(this@PressPrint)
                                dialog_delivery_partial.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                dialog_delivery_partial.setContentView(R.layout.dialog_partialdoneview)
                                dialog_delivery_partial.setCancelable(true)


                                val inputdeliveryFormat = SimpleDateFormat("yyyy-MM-dd")
                                val outputdelivryFormat = SimpleDateFormat("MM-dd-yyyy")
                                val inputdeliveryDateStr = dropsarrList.get(m).delivery_date.toString()
                                val deliverydate = inputdeliveryFormat.parse(inputdeliveryDateStr)
                                val outputdeliveryDateStr = outputdelivryFormat.format(deliverydate)

                                val inputFormat = SimpleDateFormat("yyyy-MM-dd")
                                val outputFormat = SimpleDateFormat("MM-dd-yyyy")
                                val inputDateStr = dropsarrList.get(m).date.toString()
                                val date = inputFormat.parse(inputDateStr)
                                val outputDateStr = outputFormat.format(date)


                                val tv_drop = dialog_delivery_partial.findViewById(R.id.tv_drop) as CustomTextView
                                val tv_date = dialog_delivery_partial.findViewById(R.id.tv_dropdate) as CustomTextView
                                val tv_delivery_date = dialog_delivery_partial.findViewById(R.id.tv_deliverydate) as CustomTextView
                                val tv_location = dialog_delivery_partial.findViewById(R.id.tv_delivery_location) as CustomTextView
                                val iv_signature = dialog_delivery_partial.findViewById(R.id.tv_delivery_signature) as ImageView
                                val iv_close_dialog_delivery = dialog_delivery_partial.findViewById(R.id.iv_close_dialog_delivery) as ImageView
                                tv_drop.setText(dropsarrList.get(m).term.toString())
                                tv_date.setText(outputDateStr)
                                tv_delivery_date.setText(outputdeliveryDateStr)
                                tv_location.setText(dropsarrList.get(m).delivery_location.toString())
                                Picasso.with(this@PressPrint)
                                        .load(ApiInterface.BASE_URL + "uploads/delivorysignatures/"+dropsarrList.get(m).delivery_sign)
                                        .into(iv_signature)

                                dialog_delivery_partial.show()

                                iv_close_dialog_delivery.setOnClickListener {
                                    dialog_delivery_partial.dismiss()
                                }




                            }

                        } else if (mGroupPosition > (dropsarrList.get(m).delivery_status!!.toInt())) {
                            textDropsstatus.setText("Not Started")
                            textDropsstatus!!.setBackgroundColor(resources.getColor(R.color.tab2_bg))
                        }
                    } else {
                        childViewHolder!!.ll_drops_list_id!!.visibility = View.GONE
                    }


                    textDropsstatus.setAllCaps(false)
                    textDropsstatus!!.setTextColor(resources.getColor(R.color.white))
                    childViewHolder!!.ll_drop_sno_id?.addView(textdrops_sno)
                    childViewHolder!!.ll_no_drop_id?.addView(textdrops)
                    childViewHolder!!.ll_drop_date_id?.addView(textdropsdate)
                    childViewHolder!!.ll_drops_done_id?.addView(textDropsstatus)
                }
            } else {
                childViewHolder!!.ll_drops_list_id!!.visibility = View.GONE
            }



            childViewHolder!!.btn_postcomment!!.setOnClickListener {
                comments_text = childViewHolder!!.et_comments!!.text.toString()
                assigned_task_id = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id!!

                if (comments_text.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter Comment", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("BTNPOSTCOMMENT", listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id)
                    callpostCommentsAPI(user_id, comments_text, assigned_task_id)
                }
            }

            Log.d("ASSIGNEDTASKSTATUS", mGroupPosition.toString() + "---" + listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_status)
            if (mGroupPosition.equals(0)) {
                childViewHolder!!.btn_done!!.visibility = View.VISIBLE
                if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).done_status.equals("1")) {
                    childViewHolder!!.btn_done!!.visibility = View.VISIBLE

                } else {
                    childViewHolder!!.btn_done!!.visibility = View.GONE
                }

                if (department_id.equals("3")) {
                    childViewHolder!!.ll_sheets_list_id!!.visibility = View.VISIBLE
                } else {
                    childViewHolder!!.ll_sheets_list_id!!.visibility = View.GONE
                }
            } else {
                childViewHolder!!.btn_done!!.visibility = View.GONE
                if (listDataChild[listDataHeader[groupPosition - 1]]!!.get(childPosition).assigned_task_status.equals("2")) {
                    childViewHolder!!.btn_done!!.visibility = View.VISIBLE

                }
                // childViewHolder!!.ll_sheets_list_id?.visibility=View.GONE
            }


//TODO
            //Delivery
            Log.e("DELIVERY_TEST", listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).task_name)
            if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).task_name.equals("Delivery")) {
                childViewHolder!!.btn_done!!.visibility = View.GONE

            }
            if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).task_name.equals("Pickup At Skokie")) {
                childViewHolder!!.btn_done!!.visibility = View.GONE

            }


            //TODO


            childViewHolder!!.btn_done!!.setOnClickListener {
                assigned_task_id = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id!!
                Log.d("BTN_DONETASK", listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id)

                Log.d("GROUPHEADERPOSITION", groupText + "--" + mListDataChild.size + "-------" + mGroupPosition)
                val s = mListDataChild.size - mGroupPosition
                val next_assigned_task_id: String
                if (s == 1) {
                    next_assigned_task_id = "0"
                } else {
                    next_assigned_task_id = assigned_task_id
                }

                Log.d("DONENEXTASSTASKID", "" + next_assigned_task_id)

                // callpostDoneTaskAPI( assigned_task_id,next_assigned_task_id)

                if (groupText.equals("Delivery")) {

                    dialog_action(assigned_task_id, next_assigned_task_id, (mGroupPosition + 1).toString(), "", "")

                } else {
                    //val file = File("")
                    if (department_id.equals("3")) {
                        callpostDoneTaskAPI("", assigned_task_id, next_assigned_task_id, (mGroupPosition + 1).toString())
                    } else if (department_id.equals("4")) {
                        depart_type_name = "mailing"
                        calldoneTaskMailintoDeliveryAPI("", assigned_task_id, next_assigned_task_id, depart_type_name!!, (mGroupPosition + 1).toString(), "", "")
                    } else if (department_id.equals("5")) {
                        depart_type_name = "shipping"
                        calldoneTaskMailintoDeliveryAPI("", assigned_task_id, next_assigned_task_id, depart_type_name!!, (mGroupPosition + 1).toString(), "", "")
                    } else if (department_id.equals("8")) {
                        depart_type_name = "delivery"
                        childViewHolder!!.btn_done!!.visibility = View.GONE

                        getLocation()

                        if (mLastLocation != null) run {
                            latitude = mLastLocation!!.getLatitude()
                            longitude = mLastLocation!!.getLongitude()
                            getAddress()

                        }

                        dialog_action(assigned_task_id, next_assigned_task_id, (mGroupPosition + 1).toString(), "", "")
                    } else {
                        callpostDoneTaskAPI("", assigned_task_id, next_assigned_task_id)
                    }

                }
            }


            //childViewHolder!!.mChildText!!.text = childText
            /*
		 * You have to set the onCheckChangedListener to null
		 * before restoring check states because each call to
		 * "setChecked" is accompanied by a call to the
		 * onCheckChangedListener
		*/
            // childViewHolder!!.mCheckBox!!.setOnCheckedChangeListener(null)


            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var mGroupText: TextView? = null
            internal var arrow_imageView: ImageView? = null
            //internal var mGroupCheckbox: CheckBox? = null
        }

        inner class ChildViewHolder {

            internal var tv_comments: TextView? = null
            internal var et_comments: EditText? = null
            internal var btn_postcomment: CustomTextViewBold? = null
            internal var btn_done: CustomTextViewBold? = null
            internal var IM_signature: ImageView? = null
            internal var ll_comments: LinearLayout? = null
            internal var ll_btn_cmt_done: LinearLayout? = null
            internal var no_service: TextView? = null
            internal var btn_par_done: TextView? = null

            internal var ll_s_no_id: LinearLayout? = null
            internal var ll_sheets_count_id: LinearLayout? = null
            internal var ll_partly_done_id: LinearLayout? = null
            internal var ll_sheets_list_id: LinearLayout? = null


            internal var ll_drops_list_id: LinearLayout? = null
            internal var ll_drop_sno_id: LinearLayout? = null
            internal var ll_no_drop_id: LinearLayout? = null
            internal var ll_drop_date_id: LinearLayout? = null
            internal var ll_drops_done_id: LinearLayout? = null


            internal var ll_drops_id: LinearLayout? = null
            internal var ll_drops_main_id: LinearLayout? = null
            internal var ll_delivery_view_id: LinearLayout? = null
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, DepartmentListActivity::class.java)
        //intent.putExtra("department_id", department_id)
        // intent.putExtra("department_name", department_name)
        startActivity(intent)
        finish()
        super.onBackPressed()
    }
////Digital Signature////

    // Function for Digital Signature
    fun dialog_action(ass_task_id: String, nxt_ass_task_id: String, drop_status: String, partially: String, id: String) {

        mContent = dialog.findViewById(R.id.linearLayout_signature) as LinearLayout
        mSignature = signature(applicationContext)
        mSignature.setBackgroundColor(Color.WHITE)
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = dialog.findViewById(R.id.clear) as Button
        mGetSign = dialog.findViewById(R.id.getsign) as Button
        mGetSign.isEnabled = false
        mCancel = dialog.findViewById(R.id.cancel) as Button
        view = mContent

        mClear.setOnClickListener {
            Log.v("log_tag", "Panel Cleared")
            mSignature.clear()
            mGetSign.isEnabled = false
        }

        mGetSign.setOnClickListener {
            Log.v("log_tag", "Panel Saved")
            val folder_main = "DigitSign"

            val f = File(Environment.getExternalStorageDirectory(), folder_main)
            if (!f.exists()) {
                f.mkdirs()
            }
            val DIRECTORY = Environment.getExternalStorageDirectory().path + "/DigitSign/"

            val pic_name = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
            val StoredPath = "$DIRECTORY$pic_name.png"


            view.isDrawingCacheEnabled = true
            mSignature.save(view, StoredPath, ass_task_id, nxt_ass_task_id, drop_status, partially, id)
            dialog.dismiss()
            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()

            Log.e("StoredPath_lll", StoredPath)

        }

        mCancel.setOnClickListener {
            Log.v("log_tag", "Panel Canceled")
            dialog.dismiss()
            // Calling the same class
            //recreate()
        }
        dialog.show()
    }

    inner class signature(context: Context) : View(context) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2

        init {
            paint.isAntiAlias = true
            paint.color = Color.BLACK
            paint.style = Paint.Style.STROKE
            paint.strokeJoin = Paint.Join.ROUND
            paint.strokeWidth = STROKE_WIDTH
        }

        fun save(v: View, StoredPath: String, ass_task_id: String, nxt_ass_task_id: String, drop_status: String, partially: String, id: String) {
            Log.v("log_tag", "Width: " + v.width)
            Log.v("log_tag", "Height: " + v.height)
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(bitmap!!)
            try {
                // Output the file
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)

                // Convert the output file to Image such as .png
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream)
                mFileOutStream.flush()
                mFileOutStream.close()

                Log.v("log_tag", StoredPath)
                val day: Int
                val month: Int
                val year: Int
                val second: Int
                val minute: Int
                val hour: Int
                val date = GregorianCalendar()

                day = date.get(Calendar.DAY_OF_MONTH)
                month = date.get(Calendar.MONTH)
                year = date.get(Calendar.YEAR)

                second = date.get(Calendar.SECOND)
                minute = date.get(Calendar.MINUTE)
                hour = date.get(Calendar.HOUR)

                val name = hour.toString() + "" + minute + "" + second + "" + day + "" + (month + 1) + "" + year
                val tag = "$name.jpg"
                // val fileName = StoredPath.replace(StoredPath, tag)
                val file = File(StoredPath)


                Log.v("test image", file.name + "---" + file + "---" + ass_task_id + "---" + nxt_ass_task_id)

                //calldoneTaskMailintoDeliveryAPI(file, assigned_task_id, nxt_ass_task_id,"", (mGroupPosition + 1).toString(),"","")

                if (partially.equals("partially")) {
                    callpartiallyDoneDeliverysAPIAPI("delivery", drop_status, id, ass_task_id, nxt_ass_task_id, final_gps_location_stg, (day.toString() + "-" + month + "-" + year + " " + hour + " : " + minute).toString(), file)
                } else {

                    callDoneDelivertAPI(file, ass_task_id, nxt_ass_task_id, "delivery", drop_status, final_gps_location_stg, (day.toString() + "-" + month + "-" + year + " " + hour + " : " + minute))
                }

            } catch (e: Exception) {
                Log.v("log_tag_error", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
        }

        override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
            mGetSign.isEnabled = true

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }
    }


    private fun getLocation() {

        if (isPermissionGranted) {

            try {
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }

        }

    }

    fun getAddress(latitude: Double, longitude: Double): Address? {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(this, Locale.getDefault())

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses[0]

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null

    }


    fun getAddress() {

        val locationAddress = getAddress(latitude, longitude)

        if (locationAddress != null) {
            val address = locationAddress.getAddressLine(0)
            val address1 = locationAddress.getAddressLine(1)
            val city = locationAddress.locality
            val state = locationAddress.adminArea
            val country = locationAddress.countryName
            val postalCode = locationAddress.postalCode

            var currentLocation: String

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1

                if (!TextUtils.isEmpty(city)) {
                    /* currentLocation += "\n" + city

                     if (!TextUtils.isEmpty(postalCode))
                         currentLocation += " - $postalCode"*/
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode
                }

                /*if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country*/

                final_gps_location_stg = currentLocation

                Log.e("locat", final_gps_location_stg)

            }

        }

    }

    /**
     * Creating google api client object
     */

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()

        mGoogleApiClient!!.connect()

        val mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)

        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback { locationSettingsResult ->
            val status = locationSettingsResult.status

            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS ->
                    // All location settings are satisfied. The client can initialize location requests here
                    getLocation()

                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    status.startResolutionForResult(this@PressPrint, REQUEST_CHECK_SETTINGS)

                } catch (e: IntentSender.SendIntentException) {
                    // Ignore the error.
                }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }


    }


    /**
     * Method to verify google play services on the device
     */

    private fun checkPlayServices(): Boolean {

        val googleApiAvailability = GoogleApiAvailability.getInstance()

        val resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this)

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(this, resultCode,
                        PLAY_SERVICES_REQUEST).show()
            } else {
                Toast.makeText(applicationContext,
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show()
                finish()
            }
            return false
        }
        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val states = LocationSettingsStates.fromIntent(data)
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK ->
                    // All required changes were successfully made
                    getLocation()
                Activity.RESULT_CANCELED -> {
                }
                else -> {
                }
            }// The user was asked to change settings, but chose not to
        }
    }


   /* fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val states = LocationSettingsStates.fromIntent(data)
        when (requestCode) {
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK ->
                    // All required changes were successfully made
                    getLocation()
                Activity.RESULT_CANCELED -> {
                }
                else -> {
                }
            }// The user was asked to change settings, but chose not to
        }
    }*/


    override fun onResume() {
        super.onResume()
        checkPlayServices()
    }

    /**
     * Google api callback methods
     */
    override fun onConnectionFailed(result: ConnectionResult) {
        /*  Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());*/
    }

    override fun onConnected(arg0: Bundle?) {

        // Once connected with google api, get the location
        getLocation()
    }

    override fun onConnectionSuspended(arg0: Int) {
        mGoogleApiClient!!.connect()
    }


    // Permission check functions


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }


    override fun PermissionGranted(request_code: Int) {
        Log.i("PERMISSION", "GRANTED")
        isPermissionGranted = true
    }

    override fun PartialPermissionGranted(request_code: Int, granted_permissions: java.util.ArrayList<String>) {
        Log.i("PERMISSION PARTIALLY", "GRANTED")
    }

    override fun PermissionDenied(request_code: Int) {
        Log.i("PERMISSION", "DENIED")
    }

    override fun NeverAskAgain(request_code: Int) {
        Log.i("PERMISSION", "NEVER ASK AGAIN")
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    /*fun dialog_delivery_view(drop: String, drop_date: String, deliver_date: String, drop_location: String, drop_signature: String) {

        dialog_delivery_partial = Dialog(this)
        dialog_delivery_partial.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_delivery_partial.setContentView(R.layout.dialog_partialdoneview)
        dialog_delivery_partial.setCancelable(true)


        val tv_drop = dialog_delivery_partial.findViewById(R.id.tv_drop) as CustomTextView
        val tv_date = dialog_delivery_partial.findViewById(R.id.tv_date) as CustomTextView
        val tv_delivery_date = dialog_delivery_partial.findViewById(R.id.tv_drop_date) as CustomTextView
        val tv_location = dialog_delivery_partial.findViewById(R.id.tv_location) as CustomTextView
        val iv_signature = dialog_delivery_partial.findViewById(R.id.iv_signature) as ImageView


        tv_drop.setText(drop)
        tv_date.setText(drop_date)
        tv_delivery_date.setText(deliver_date)
        tv_location.setText(drop_location)
        Picasso.with(this)
                .load(ApiInterface.BASE_URL + "uploads/delivorysignatures/"+drop_signature)
                .into(iv_signature)

        //dialog_delivery_partial.show()

    }*/

    override fun onDestroy() {
        super.onDestroy()
        mIsDestroyed = true

        if (my_loader != null && my_loader.isShowing()) {
            my_loader.dismiss()
        }
    }

    public override fun onPause() {
        super.onPause()

        if (my_loader != null)
            my_loader.dismiss()
        //my_loader = null
    }


}
