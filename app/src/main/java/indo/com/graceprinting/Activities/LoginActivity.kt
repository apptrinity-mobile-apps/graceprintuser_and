package indo.com.graceprinting.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface



import indo.com.graceprinting.Helper.ConnectionManager
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.Model.APIResponse.UserLoginResponse
import indo.com.graceprinting.Model.APIResponse.User_info
import indo.com.graceprinting.Model.DeviceTokenModel
import indo.com.graceprinting.R

import retrofit2.Call
import retrofit2.Callback


class LoginActivity : AppCompatActivity() {
    internal lateinit var et_user_name_id: EditText
    internal var et_pass_id: EditText? = null
    internal lateinit var tv_forgot_pass_id: CustomTextView
    internal lateinit var tv_sign_in_id: CustomTextViewBold
    internal lateinit var tv_forgot_user_name_id: TextView
    internal lateinit var user_name_stg: String
    internal lateinit var password_stg: String
    internal lateinit var sessionManager: SessionManager
    internal lateinit var tv_register: TextView
    internal lateinit var my_loader: Dialog
    internal lateinit var deviceTokenModel: DeviceTokenModel
    internal lateinit var refreshedToken: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)
        if (sessionManager.isLoggedIn) {
            val home_intent = Intent(this@LoginActivity, DepartmentListActivity::class.java)
            finish()
            startActivity(home_intent)
        } else {
            setContentView(R.layout.activity_login)
            myloading()

            et_user_name_id = findViewById(R.id.et_user_name) as EditText
            et_pass_id = findViewById(R.id.et_pass_id) as EditText
            tv_forgot_pass_id = findViewById(R.id.tv_forgot_pass_id)
            tv_sign_in_id = findViewById(R.id.tv_sign_in_id)

            tv_register = findViewById(R.id.tv_register) as TextView
            var rootview = findViewById(R.id.ll_login_id) as LinearLayout

            tv_sign_in_id.setOnClickListener(View.OnClickListener {
                user_name_stg = et_user_name_id.getText().toString().trim({ it <= ' ' })
                password_stg = et_pass_id!!.getText().toString().trim({ it <= ' ' })
                if (hasValidCredentials()) {
                    if (ConnectionManager.checkConnection(applicationContext)) {
                        var iid = FirebaseInstanceId.getInstance()
                        var refreshedToken = iid.token

                        while (refreshedToken == null) {
                            iid = FirebaseInstanceId.getInstance()
                            refreshedToken = iid.token
                        }
                        Log.e("refreshedToken",refreshedToken );

                        callLoginAPI(user_name_stg, password_stg,refreshedToken.toString(),"ANDROID")
                    } else {
                        ConnectionManager.snackBarNetworkAlert_LinearLayout(rootview, applicationContext)
                    }
                }
            })

            tv_forgot_pass_id.setOnClickListener(View.OnClickListener {
              /*  val intent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                intent.putExtra("forgot", "password")
                startActivity(intent)*/
            })


        }


    }



    private fun myloading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }


    private fun callLoginAPI(user_name_stg: String, password_stg: String,refreshedToken: String,device_type:String) {
        my_loader.show()
        val apiService = ApiInterface.create()
        val call = apiService.login(user_name_stg, password_stg,refreshedToken,"ANDROID")
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<UserLoginResponse> {
            override fun onResponse(call: Call<UserLoginResponse>, response: retrofit2.Response<UserLoginResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Address", "Result : " + response.body()!!.status)
                    if (response.body()!!.status.equals("1") && response.body()!!.user_info != null) {
                        storeLoginData(response.body()!!.user_info)
                        val intent = Intent(this@LoginActivity, DepartmentListActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    } else {
                        Toast.makeText(applicationContext, "Invalid Username and Password", Toast.LENGTH_SHORT).show()
                    }

                }
            }

            override fun onFailure(call: Call<UserLoginResponse>, t: Throwable) {
                Log.w("Response_Product", "Result : Failed")
            }
        })
    }

    private fun storeLoginData(user_info: User_info?) {
        if (user_info != null) {
            try {
                sessionManager.createLoginSession(user_info.id!!, user_info.department_id!!,"", user_info.user_name!!, user_info.user_email!!, user_info.user_mobile_number!!)
                /*if (user_info.id != null && user_info.department_id != null && user_info.department_name != null && user_info.user_name != null && user_info.user_email != null && user_info.user_mobile_number != null) {

                }*/
            } catch (e: Exception) {
                Log.w("exception", "" + e.printStackTrace())
            }

        }

    }


    private fun hasValidCredentials(): Boolean {
        if (TextUtils.isEmpty(user_name_stg))
            et_user_name_id.setError("Username Required")
        else if (TextUtils.isEmpty(password_stg))
            et_pass_id!!.setError("Password Required")
        else if (et_pass_id != null && et_pass_id!!.length() < 6)
            et_pass_id!!.setError("Invalid Password")
        else
            return true
        return false
    }

    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
