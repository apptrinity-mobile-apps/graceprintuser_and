package indo.com.graceprinting.Helper

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import indo.com.graceprinting.Activities.LoginActivity
import java.util.*


/**
 * Created by indobytes21 on 2/9/2017.
 */

class SessionManager// Constructor
(// Context
        internal var _context: Context) {
    internal var pref: SharedPreferences
    // Editor for Shared preferences
    internal var editor: SharedPreferences.Editor
    // Shared pref mode
    internal var PRIVATE_MODE = 0
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)
    val isUserId :String
        get() = pref.getString(PROFILE_ID,"")
    val isUserName :String
        get() = pref.getString(PROFILE_USERNAME,"User Name")

    // user name
    // user email id
    //user.put(PROFILE_WEB_ID, pref.getString(PROFILE_WEB_ID, null));
    // return user
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[PROFILE_ID] = pref.getString(PROFILE_ID, "")
            user[PROFILE_EMAIL] = pref.getString(PROFILE_EMAIL, "")
            user[PROFILE_USERNAME] = pref.getString(PROFILE_USERNAME, "")
            user[DEPARTMENTID] = pref.getString(DEPARTMENTID, "")
            user[DEPARTMENTNAME] = pref.getString(DEPARTMENTNAME, "")
            user[PROFILE_PHONE_NUMBER] = pref.getString(PROFILE_PHONE_NUMBER, "")

            return user
        }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(userid: String, department_id: String, department_name: String,username: String, email: String, phone_number: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        // Storing name in pref
        editor.putString(PROFILE_ID, userid)
        editor.putString(PROFILE_EMAIL, email)
        editor.putString(PROFILE_USERNAME, username)
        editor.putString(DEPARTMENTID, department_id)
        editor.putString(DEPARTMENTNAME, department_name)
        editor.putString(PROFILE_PHONE_NUMBER, phone_number)

        // commit changes
        editor.commit()
    }


    fun createDepartmentSession(department_id: String) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)
        // Storing name in pref
        editor.putString(DEPARTMENTID, department_id)


        // commit changes
        editor.commit()
    }


    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            // Add new Flag to start new Activity
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            // Staring Login Activity
            _context.startActivity(i)
        }
    }

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()
        // After logout redirect user to Loing Activity
        val i = Intent(_context, LoginActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }


    companion object {

        var PROFILE_ID = "userId"
        var PROFILE_EMAIL = "email"
        var PROFILE_USERNAME = "username"
        var DEPARTMENTID = "departmentid"
        var DEPARTMENTNAME = "departmentname"
        var PROFILE_PHONE_NUMBER = "phone_number"

        var IS_LOGIN = "IsLoggedIn"
        var PREF_NAME = "graceprint"
    }
}
