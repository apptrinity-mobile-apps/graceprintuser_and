package indo.com.graceprinting.Fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.indobytes.a4printuser.Helper.CustomTextView
import com.indobytes.a4printuser.Helper.CustomTextViewBold
import com.indobytes.a4printuser.model.ApiInterface
import indo.com.graceprinting.Activities.PressPrint2
import indo.com.graceprinting.Activities.SheetsActivity2
import indo.com.graceprinting.Helper.SessionManager
import indo.com.graceprinting.Model.APIResponse.GetJobsResponse
import indo.com.graceprinting.Model.APIResponse.JobsListDataResponse
import indo.com.graceprinting.R
import retrofit2.Call
import retrofit2.Callback
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class CompletedFragment : Fragment() {
    private var type: String? = null
    private var param2: String? = null

    private var expListView: ExpandableListView? = null
    private var no_service: TextView? = null
    lateinit var listAdapter: ExpListViewAdapterWithCheckbox
    lateinit var listDataHeader: ArrayList<String>
    lateinit var listDataHeader1: ArrayList<String>
    lateinit var listDataHeaderCustomer: ArrayList<String>
    lateinit var listDataHeader_jobpart: ArrayList<String>
    lateinit var listDataChild: HashMap<String, ArrayList<JobsListDataResponse>>
    lateinit var list:  List<JobsListDataResponse>

    private var lastExpandedPosition = -1

    internal lateinit var sessionManager: SessionManager
    internal lateinit var user_id: String
    internal var department_id: String =""
    internal lateinit var my_loader: Dialog
    internal var printer_id: String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val rootView = LayoutInflater.from(this.activity).inflate(R.layout.fragment_first, null, false)

        myloading()

        expListView = rootView!!.findViewById(R.id.lvExp) as ExpandableListView
        no_service = rootView!!.findViewById(R.id.no_service) as TextView


        sessionManager = SessionManager(this.activity!!)
        val user = sessionManager.userDetails

        user_id = user.get(SessionManager.PROFILE_ID).toString()
        Log.e("user_id",user_id);

       /* department_id = user.get(SessionManager.DEPARTMENTID).toString()
        Log.e("department_id",department_id);*/

        if(activity!!.intent != null && activity!!.intent.getStringExtra("department_id")!=null){
            department_id = activity!!.intent.getStringExtra("department_id")
        }

        if(activity!!.intent != null && activity!!.intent.getStringExtra("printerid")!=null){
            printer_id = activity!!.intent.getStringExtra("printerid")
        }
        //  department_id = activity!!.intent.getStringExtra("department_id")
        Log.e("department_id CHAITANYA", department_id+"---"+printer_id);


        callgetJobsAPI()



        // Listview Group click listener
        expListView!!.setOnGroupClickListener { parent, v, groupPosition, id ->
            // Toast.makeText(getApplicationContext(),
            // "Group Clicked " + listDataHeader.get(groupPosition),
            // Toast.LENGTH_SHORT).show();

            false
        }

        // Listview Group expanded listener
        expListView!!.setOnGroupExpandListener { groupPosition ->
            /*Toast.makeText(this.activity,
                    listDataHeader[groupPosition] + " Expanded",
                    Toast.LENGTH_SHORT).show()*/
            if (lastExpandedPosition != -1
                    && groupPosition != lastExpandedPosition) {
                expListView!!.collapseGroup(lastExpandedPosition);
            }
            lastExpandedPosition = groupPosition;
        }

        // Listview Group collasped listener
        expListView!!.setOnGroupCollapseListener { groupPosition ->
           /* Toast.makeText(this.activity,
                    listDataHeader[groupPosition] + " Collapsed",
                    Toast.LENGTH_SHORT).show()*/
        }

        // Listview on child click listener
        expListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            // TODO Auto-generated method stub
           /* Toast.makeText(
                    this.activity,
                    listDataHeader[groupPosition]
                            + " : "
                            + listDataChild[listDataHeader[groupPosition]]!!.get(
                            childPosition), Toast.LENGTH_SHORT)
                    .show()*/
            false
        }





        return rootView

    }


    private fun myloading() {
        my_loader = Dialog(this.activity)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

    private fun callgetJobsAPI() {
        my_loader.show()

        val apiService = ApiInterface.create()
        val call = apiService.getJobs(user_id,"3",department_id,printer_id)
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<GetJobsResponse> {
            override fun onResponse(call: Call<GetJobsResponse>, response: retrofit2.Response<GetJobsResponse>?) {
                if (response != null) {
                    my_loader.dismiss()
                    Log.w("Result_Order_details", response.body().toString())
                    if(response.body()!!.status.equals("1") && response.body()!!.data!=null) {
                        listDataHeader = ArrayList()
                        listDataHeader1 = ArrayList()
                        listDataHeaderCustomer = ArrayList()
                        listDataHeader_jobpart = ArrayList()
                        listDataChild = HashMap()
                        val list: Array<JobsListDataResponse>? = response.body()!!.data!!
                        if(list!!.size>0){
                            for (j in 0 until list.size) {

                                listDataHeader1.add(list.get(j).job_id.toString())
                                listDataHeaderCustomer.add(list.get(j).custmer.toString())
                                listDataHeader_jobpart.add(list.get(j).job_part.toString())
                                listDataHeader.add(list.get(j).id.toString())

                                val top = ArrayList<JobsListDataResponse>()
                                top.add(list.get(j))
                                Log.w("Result_Order_details", list.get(j).toString())

                                listDataChild.put(listDataHeader[j],top)
                            }
                            Log.e("stdferer",""+listDataChild.size)


                            listAdapter = ExpListViewAdapterWithCheckbox(activity!!, listDataHeader, listDataChild)

                            // setting list adapter
                            expListView!!.setAdapter(listAdapter)
                            listAdapter.notifyDataSetChanged()
                        }



                    }else if(response.body()!!.status.equals("2")){
                        no_service!!.text = "No Data Found"
                        no_service!!.visibility = View.VISIBLE
                    }

                }
            }

            override fun onFailure(call: Call<GetJobsResponse>, t: Throwable) {
                Log.w("Result_Order_details",t.toString())
            }
        })
    }




    inner class ExpListViewAdapterWithCheckbox/*  Here's the constructor we'll use to pass in our calling
         *  activity's context, group items, and child items
        */
    (// Define activity context
            private val mContext: Context, // ArrayList that is what each key in the above
            // hashmap points to
            private val mListDataGroup: ArrayList<String>, /*
         * Here we have a Hashmap containing a String key
         * (can be Integer or other type but I was testing
         * with contacts so I used contact name as the key)
        */
            private val mListDataChild: HashMap<String, ArrayList<JobsListDataResponse>>) : BaseExpandableListAdapter() {

        // Hashmap for keeping track of our checkbox check states
        private val mChildCheckStates: HashMap<Int, BooleanArray>
        private val mGroupCheckStates: HashMap<Int, BooleanArray>

        // Our getChildView & getGroupView use the viewholder patter
        // Here are the viewholders defined, the inner classes are
        // at the bottom
        private var childViewHolder: ChildViewHolder? = null
        private var groupViewHolder: GroupViewHolder? = null


        private val colors = intArrayOf(Color.parseColor("#fff9ef"), Color.parseColor("#eefbff"), Color.parseColor("#fff3eb"), Color.parseColor("#ffeffa"), Color.parseColor("#ffece1"), Color.parseColor("#ebffec"))

        private val list_images = intArrayOf(R.drawable.bg1,R.drawable.bg2,R.drawable.bg3,R.drawable.bg4,R.drawable.bg5,R.drawable.bg6)

        /*
              *  For the purpose of this document, I'm only using a single
         *	textview in the group (parent) and child, but you're limited only
         *	by your XML view for each group item :)
        */
        private var groupText: String? = null
        private var childText: String? = null
        private var str_width_height: String? = null

        init {

            // Initialize our hashmap containing our check states here
            mChildCheckStates = HashMap()
            mGroupCheckStates = HashMap()
        }

        fun getNumberOfCheckedItemsInGroup(mGroupPosition: Int): Int {
            val getChecked = mChildCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {

                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        fun getNumberOfGroupCheckedItems(mGroupPosition: Int): Int {
            val getChecked = mGroupCheckStates[mGroupPosition]
            var count = 0
            if (getChecked != null) {
                for (j in getChecked.indices) {
                    if (getChecked[j] == true) count++
                }
            }
            return count
        }

        override fun getGroupCount(): Int {
            return mListDataGroup.size
        }

        /*
         * This defaults to "public object getGroup" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getGroup(groupPosition: Int): String {
            return mListDataGroup[groupPosition]
        }

        override fun getGroupId(groupPosition: Int): Long {
            return groupPosition.toLong()
        }


        override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                                  convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListGroupItems".
            //  Here is where I call the getter to get that text
            groupText = getGroup(groupPosition)

            if (convertView == null) {

                val inflater = mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.list_group, null)

                // Initialize the GroupViewHolder defined at the bottom of this document
                groupViewHolder = GroupViewHolder()

                groupViewHolder!!.mGroupText = convertView!!.findViewById(R.id.lblListHeader) as TextView
                groupViewHolder!!.mGroupText1 = convertView!!.findViewById(R.id.lblListHeader1) as TextView
                groupViewHolder!!.mbg_layout = convertView!!.findViewById(R.id.ll_bg) as RelativeLayout
                groupViewHolder!!.card_view = convertView!!.findViewById(R.id.card_view) as CardView
                groupViewHolder!!.ll_header_joblist = convertView!!.findViewById(R.id.ll_header_joblist) as LinearLayout
                groupViewHolder!!.tv_customer_name = convertView!!.findViewById(R.id.tv_customer_name) as CustomTextViewBold
                // groupViewHolder!!.iv_notification = convertView!!.findViewById(R.id.iv_notification) as ImageView
                // groupViewHolder!!.image_move_id = convertView!!.findViewById(R.id.image_move_id) as ImageView
                // groupViewHolder!!.img_layout = convertView!!.findViewById(R.id.lblListHeader_image) as ImageView
                //groupViewHolder!!.img_layout = convertView!!.findViewById(R.id.image_move_id) as ImageView

                val colorPos = groupPosition % colors.size
                val imagePos = groupPosition % list_images.size
                groupViewHolder!!.card_view!!.setBackgroundColor(colors[colorPos])
               // groupViewHolder!!.ll_header_joblist!!.setBackgroundResource(list_images[imagePos])

                if(listDataHeader_jobpart.get(groupPosition).equals("1")){
                    groupViewHolder!!.ll_header_joblist!!.setBackgroundResource(R.drawable.bg1)
                }else if(listDataHeader_jobpart.get(groupPosition).equals("2")){
                    groupViewHolder!!.ll_header_joblist!!.setBackgroundResource(R.drawable.bg2)
                }else if(listDataHeader_jobpart.get(groupPosition).equals("3")){
                    groupViewHolder!!.ll_header_joblist!!.setBackgroundResource(R.drawable.bg4)
                }else if(listDataHeader_jobpart.get(groupPosition).equals("4")){
                    groupViewHolder!!.ll_header_joblist!!.setBackgroundResource(R.drawable.bg5)
                }
               // groupViewHolder!!.mbg_layout!!.setBackgroundColor(resources.getColor(R.color.tab4_bg))


                convertView.tag = groupViewHolder
            } else {

                groupViewHolder = convertView.tag as GroupViewHolder
            }

            //groupViewHolder!!.mGroupText!!.text = groupText

            groupViewHolder!!.mGroupText!!.text = "Job - " +listDataHeader1.get(groupPosition)
            groupViewHolder!!.mGroupText1!!.text = "Job Part " +listDataHeader_jobpart.get(groupPosition)
            groupViewHolder!!.tv_customer_name!!.text = "Customer :  " +listDataHeaderCustomer.get(groupPosition)


            return convertView
        }

        override fun getChildrenCount(groupPosition: Int): Int {
            return mListDataChild[mListDataGroup[groupPosition]]!!.size
        }

        /*
         * This defaults to "public object getChild" if you auto import the methods
         * I've always make a point to change it from "object" to whatever item
         * I passed through the constructor
        */
        override fun getChild(groupPosition: Int, childPosition: Int): JobsListDataResponse {
            return mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition)
        }

        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
            return childPosition.toLong()
        }

        override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            val mGroupPosition = groupPosition

            //  I passed a text string into an activity holding a getter/setter
            //  which I passed in through "ExpListChildItems".
            //  Here is where I call the getter to get that text
            //childText = getChild(groupPosition, childPosition)
            Log.e("mListDataChild",""+mListDataChild.size)

            // val list: Array<JobsListDataResponse>? = mListDataChild[mListDataGroup[groupPosition]]

            // list =  mListDataChild[mListDataGroup[groupPosition]] as List<*> as List<JobsListDataResponse>
            Log.e("asasas",""+ mListDataChild[mListDataGroup[groupPosition]]!!.get(childPosition).job_id);

           /* Toast.makeText(this.mContext,listDataHeader[groupPosition] + " : "
                    + listDataChild[listDataHeader[groupPosition]]!!.get(
                    childPosition).assigned_id, Toast.LENGTH_SHORT ).show()*/



            if (convertView == null) {

                val inflater = this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.child_list_item_jobs, null)

                childViewHolder = ChildViewHolder()
                childViewHolder!!.ll_date_terms_value = convertView!!.findViewById(R.id.layout_date_terms_value) as LinearLayout
                childViewHolder!!.ll_layout_date_terms = convertView!!.findViewById(R.id.layout_date_terms) as LinearLayout
                childViewHolder!!.tv_customer = convertView!!.findViewById(R.id.tv_customer)
                childViewHolder!!.tv_jobpart = convertView!!.findViewById(R.id.tv_jobpart)
                childViewHolder!!.tv_description = convertView!!.findViewById(R.id.tv_description)
                childViewHolder!!.tv_jobtitle = convertView!!.findViewById(R.id.tv_jobtitle)
                childViewHolder!!.tv_parttitle = convertView!!.findViewById(R.id.tv_parttitle)
                childViewHolder!!.tv_promisedate = convertView!!.findViewById(R.id.tv_promisedate)
                childViewHolder!!.tv_earlydate = convertView!!.findViewById(R.id.tv_earlydate)
                childViewHolder!!.tv_status = convertView!!.findViewById(R.id.tv_status)
                childViewHolder!!.tv_scheduled = convertView!!.findViewById(R.id.tv_scheduled)
                childViewHolder!!.tv_critdate = convertView!!.findViewById(R.id.tv_critdate)
                childViewHolder!!.tv_duetime = convertView!!.findViewById(R.id.tv_duetime)
                childViewHolder!!.tv_lastactcode = convertView!!.findViewById(R.id.tv_lastactcode)
                childViewHolder!!.btn_start = convertView!!.findViewById(R.id.btn_start)
                childViewHolder!!.btn_start!!.visibility=View.VISIBLE
                childViewHolder!!.btn_start!!.setBackground(resources.getDrawable(R.drawable.curved_button_bg))
                childViewHolder!!.btn_start!!.setText("View")

                childViewHolder!!.tv_printertype = convertView!!.findViewById(R.id.tv_printertype)
                childViewHolder!!.tv_optiontype = convertView!!.findViewById(R.id.tv_optiontype)
                childViewHolder!!.tv_papername = convertView!!.findViewById(R.id.tv_papername)
                childViewHolder!!.tv_txtname = convertView!!.findViewById(R.id.tv_txtname)
                childViewHolder!!.tv_subtxtname = convertView!!.findViewById(R.id.tv_subtxtname)
                childViewHolder!!.tv_weight = convertView!!.findViewById(R.id.tv_weight)
                childViewHolder!!.tv_size = convertView!!.findViewById(R.id.tv_size)
                childViewHolder!!.tv_allotedsheet = convertView!!.findViewById(R.id.tv_allotedsheet)
                childViewHolder!!.tv_mailqty = convertView!!.findViewById(R.id.tv_mailqty)
                childViewHolder!!.tv_noterms = convertView!!.findViewById(R.id.tv_noterms)


                childViewHolder!!.ll_row2 = convertView!!.findViewById(R.id.ll_row2)
                childViewHolder!!.ll_row4 = convertView!!.findViewById(R.id.ll_row4)
                childViewHolder!!.ll_row6 = convertView!!.findViewById(R.id.ll_row6)
                childViewHolder!!.ll_row8 = convertView!!.findViewById(R.id.ll_row8)
                childViewHolder!!.ll_row10 = convertView!!.findViewById(R.id.ll_row10)
                childViewHolder!!.ll_row12 = convertView!!.findViewById(R.id.ll_row12)
                childViewHolder!!.ll_row14 = convertView!!.findViewById(R.id.ll_row14)
                childViewHolder!!.ll_row16 = convertView!!.findViewById(R.id.ll_row16)
                childViewHolder!!.ll_row18 = convertView!!.findViewById(R.id.ll_row18)


                /*childViewHolder!!.ll_row2!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row4!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row6!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row8!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row10!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row12!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row14!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row16!!.setBackgroundColor(resources.getColor(R.color.completed_row))
                childViewHolder!!.ll_row18!!.setBackgroundColor(resources.getColor(R.color.completed_row))*/


                val start_status = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).start_status
                if(start_status!!.equals("3")){
                    childViewHolder!!.btn_start!!.visibility = View.VISIBLE
                }else{
                    childViewHolder!!.btn_start!!.visibility = View.GONE
                }


                childViewHolder!!.btn_start!!.setOnClickListener(View.OnClickListener {


                    if(department_id.equals("2")){
                        val home_intent = Intent(activity, SheetsActivity2::class.java)
                        home_intent.putExtra("job_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_id)
                        home_intent.putExtra("active_continue","continue")
                        home_intent.putExtra("assigned_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_id)
                        //  home_intent.putExtra("assigned_task_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_task_id)
                        home_intent.putExtra("allotted_sheets",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).alloted_sheets)
                        home_intent.putExtra("used_sheets",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).used_sheets)
                        home_intent.putExtra("job_department_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_department_id)
                        home_intent.putExtra("department_id", department_id)
                        startActivity(home_intent)
                    }else{
                        val home_intent = Intent(activity, PressPrint2::class.java)
                        home_intent.putExtra("job_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_id)
                        home_intent.putExtra("active_continue","continue")
                        home_intent.putExtra("assigned_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).assigned_id)
                        home_intent.putExtra("job_department_id",listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_department_id)
                        home_intent.putExtra("department_id", department_id)
                        startActivity(home_intent)
                    }

                })


                convertView.setTag(R.layout.child_list_item_jobs, childViewHolder)

            } else {

                childViewHolder = convertView
                        .getTag(R.layout.child_list_item_jobs) as ChildViewHolder
            }
            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "MM/dd/yyyy h:mm a"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)
            childViewHolder!!.tv_customer!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).custmer
            childViewHolder!!.tv_jobpart!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_part
            childViewHolder!!.tv_description!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_description
            childViewHolder!!.tv_jobtitle!!.text = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).description
            childViewHolder!!.tv_parttitle!!.text = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).job_title
            val promisedate = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).promise_date

            if (promisedate != null) {

                var date: Date? = null
                var promisedate_val: String? = null
                try {
                    date = inputFormat.parse(promisedate)
                    promisedate_val = outputFormat.format(date)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                childViewHolder!!.tv_promisedate!!.text = promisedate_val
            }

            val earliestdate = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).earliest_start_date

            if (earliestdate != null) {

                var date: Date? = null
                var earliestdate_val: String? = null
                try {
                    date = inputFormat.parse(promisedate)
                    earliestdate_val = outputFormat.format(date)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                childViewHolder!!.tv_earlydate!!.text = earliestdate_val
            }
            childViewHolder!!.tv_status!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).status
            childViewHolder!!.tv_scheduled!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).scheduled
            childViewHolder!!.tv_critdate!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).crit_start_date
            childViewHolder!!.tv_duetime!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).due_time
            childViewHolder!!.tv_lastactcode!!.text =  listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).last_act_code


            if (listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).width !=null){
                str_width_height = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).width+"*"+listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).height
            }else{
                str_width_height = ""
            }
            childViewHolder!!.tv_printertype!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).printer_type
            childViewHolder!!.tv_optiontype!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).option_type
            childViewHolder!!.tv_papername!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).paper_name
            childViewHolder!!.tv_txtname!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).text_name
            childViewHolder!!.tv_subtxtname!!.text  =listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).sub_text_name
            childViewHolder!!.tv_weight!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).weight
            childViewHolder!!.tv_size!!.text  = str_width_height
            childViewHolder!!.tv_allotedsheet!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).alloted_sheets
            childViewHolder!!.tv_mailqty!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).mailing_qty
            childViewHolder!!.tv_noterms!!.text  = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).no_of_terms

            val term_list = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).terms
            val termdate_list = listDataChild[listDataHeader[groupPosition]]!!.get(childPosition).term_date
            Log.d("term_list", ""+term_list!!.size)
            val inputPattern1 = "yyyy-MM-dd"
            val outputPattern1 = "MM/dd/yyyy"
            val inputFormat1 = SimpleDateFormat(inputPattern1)
            val outputFormat1 = SimpleDateFormat(outputPattern1)
            if(term_list.size>0) {
                childViewHolder!!.ll_date_terms_value!!.removeAllViews()
            for (k in 0 until term_list!!.size){

                childViewHolder!!.ll2 = LinearLayout(mContext)
                childViewHolder!!.ll2!!.setOrientation(LinearLayout.VERTICAL)
                val layout2 = LayoutInflater.from(mContext).inflate(R.layout.layout_date_terms, childViewHolder!!.ll_date_terms_value, false)

                val tv_date_value = layout2.findViewById(R.id.tv_date_value) as TextView
                val tv_date_term_value = layout2.findViewById(R.id.tv_date_term_value) as TextView

                tv_date_value.setText(term_list.get(k))
                var date: Date? = null
                var termsdate_val: String? = null
                try {
                    date = inputFormat1.parse(termdate_list!!.get(k))
                    termsdate_val = outputFormat1.format(date)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }


                tv_date_term_value.setText(termsdate_val)


                childViewHolder!!.ll2!!.addView(layout2)
                childViewHolder!!.ll_date_terms_value!!.addView(childViewHolder!!.ll2)
            }
            }else{

                childViewHolder!!.ll_date_terms_value!!.removeAllViews()
                childViewHolder!!.ll_layout_date_terms!!.visibility = View.GONE
            }

            /*
		 * You have to set the onCheckChangedListener to null
		 * before restoring check states because each call to
		 * "setChecked" is accompanied by a call to the
		 * onCheckChangedListener
		*/
            // childViewHolder!!.mCheckBox!!.setOnCheckedChangeListener(null)





            return convertView
        }

        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
            return false
        }

        override fun hasStableIds(): Boolean {
            return false
        }

        inner class GroupViewHolder {

            internal var mGroupText: TextView? = null
            internal var mGroupText1: TextView? = null
            internal var mbg_layout: RelativeLayout? = null
            internal var card_view: CardView? = null
            internal var img_layout: ImageView? = null
            internal var iv_notification: ImageView? = null
            internal var image_move_id: ImageView? = null
            internal var ll_header_joblist: LinearLayout? = null
            internal var tv_customer_name: CustomTextViewBold? = null
            //internal var mGroupCheckbox: CheckBox? = null
        }

        inner class ChildViewHolder {

            internal var tv_customer: CustomTextView? = null
            internal var tv_jobpart: CustomTextView? = null
            internal var tv_description: CustomTextView? = null
            internal var tv_jobtitle: CustomTextView? = null
            internal var tv_parttitle: CustomTextView? = null
            internal var tv_promisedate: CustomTextView? = null
            internal var tv_earlydate: CustomTextView? = null
            internal var tv_status: CustomTextView? = null
            internal var tv_scheduled: CustomTextView? = null
            internal var tv_critdate: CustomTextView? = null
            internal var tv_duetime: CustomTextView? = null
            internal var tv_lastactcode: CustomTextView? = null

            internal var tv_printertype: TextView? = null
            internal var tv_optiontype: TextView? = null
            internal var tv_papername: TextView? = null
            internal var tv_txtname: TextView? = null
            internal var tv_subtxtname: TextView? = null
            internal var tv_weight: TextView? = null
            internal var tv_size: TextView? = null
            internal var tv_allotedsheet: TextView? = null
            internal var tv_mailqty: TextView? = null
            internal var tv_noterms: TextView? = null


            internal var ll_row2: LinearLayout? = null
            internal var ll_row4: LinearLayout? = null
            internal var ll_row6: LinearLayout? = null
            internal var ll_row8: LinearLayout? = null
            internal var ll_row10: LinearLayout? = null
            internal var ll_row12: LinearLayout? = null
            internal var ll_row14: LinearLayout? = null
            internal var ll_row16: LinearLayout? = null
            internal var ll_row18: LinearLayout? = null

            internal var  ll2:LinearLayout? = null
            internal var  ll_date_terms_value:LinearLayout? = null
            internal var  layout_date_terms:LinearLayout? = null
            internal var  ll_layout_date_terms:LinearLayout? = null
            internal var btn_start: CustomTextView? = null
            //internal var mCheckBox: CheckBox? = null
        }
    }



}