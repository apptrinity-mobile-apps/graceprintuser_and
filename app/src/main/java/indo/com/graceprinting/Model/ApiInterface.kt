package com.indobytes.a4printuser.model


import com.google.gson.Gson
import indo.com.graceprinting.Model.APIResponse.*
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


/**
 * Created by indo67 on 4/13/2018.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/login")
    abstract fun login(@Field("user_email") username: String,
                       @Field("password") password: String, @Field("mobile_token") mobile_token: String,
                       @Field("device_type") device_type: String): Call<UserLoginResponse>

    @FormUrlEncoded
    @POST("api/getJobs")
        abstract fun getJobs(@Field("user_id") user_id: String, @Field("status") status: String, @Field("department_id") department_id: String, @Field("printer_id") printerid: String): Call<GetJobsResponse>

    @FormUrlEncoded
    @POST("api/startJob")
    abstract fun startJob(@Field("assigned_id") assigned_id: String, @Field("job_department_id") job_department_id: String): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/getSingleJob")
    abstract fun getSingleJob(@Field("assigned_id") assigned_id: String, @Field("job_department_id") job_department_id: String): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/postTaskComment")
    abstract fun postTaskComment(@Field("assigned_task_id") assigned_task_id: String, @Field("user_id") user_id: String, @Field("comment") comment: String): Call<StartJobResponse>


    @FormUrlEncoded
    @POST("api/doneTask")
    abstract fun doneTask(@Field("delivery_sign") image_name: String, @Field("assigned_task_id") assigned_task_id: String, @Field("next_assigned_task_id") next_assigned_task_id: String, @Field("used_sheets") used_sheets: String): Call<StartJobResponse>


    @Multipart
    @POST("api/doneTask")
    abstract fun doneTask1(@Part file: MultipartBody.Part,
                           @Part("assigned_task_id") assigned_task_id_val: RequestBody,
                           @Part("next_assigned_task_id") next_assigned_task_id_val: RequestBody,
                           @Part("used_sheets") used_sheets: RequestBody,
                           @Field("delivery_location") delivery_location: String,
                           @Field("delivery_date") delivery_date: String): Call<StartJobResponse>


    @FormUrlEncoded
    @POST("api/doneTask")
    abstract fun doneTaskBindery(@Field("delivery_sign") image_name: String, @Field("assigned_task_id") assigned_task_id: String, @Field("next_assigned_task_id") next_assigned_task_id: String, @Field("used_sheets") used_sheets: String, @Field("par_status") par_status: String): Call<StartJobResponse>


    @FormUrlEncoded
    @POST("api/doneTask")
    abstract fun doneTaskMailintoDelivery(@Field("delivery_sign") image_name: String,
                                          @Field("assigned_task_id") assigned_task_id: String,
                                          @Field("next_assigned_task_id") next_assigned_task_id: String,
                                          @Field("type") type: String,
                                          @Field("drop_status") drop_status: String,
                                          @Field("delivery_location") delivery_location: String,
                                          @Field("delivery_date") delivery_date: String): Call<StartJobResponse>

    @Multipart
    @POST("api/doneTask")
    abstract fun doneTaskDelivery(@Part file: MultipartBody.Part,
                                  @Part("assigned_task_id") assigned_task_id: RequestBody,
                                  @Part("next_assigned_task_id") next_assigned_task_id: RequestBody,
                                  @Part("type") type: RequestBody,
                                  @Part("drop_status") drop_status: RequestBody,
                                  @Part("delivery_location") delivery_location: RequestBody,
                                  @Part("delivery_date") delivery_date: RequestBody): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/partiallyDoneTask")
    abstract fun partiallyDoneTaskAPI(@Field("assigned_task_id") assigned_task_id: String, @Field("next_assigned_task_id") next_assigned_task_id: String, @Field("used_sheets") used_sheets: String): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/partiallyDonesheets")
    abstract fun partiallyDonesheetsAPI(@Field("par_id") par_id: String, @Field("status") status: String): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/partiallyDonedrops")
    abstract fun partiallyDonedropsAPI(@Field("type") type: String,
                                       @Field("status") status: String,
                                       @Field("id") id: String,
                                       @Field("assigned_task_id") assigned_task_id: String,
                                       @Field("next_assigned_task_id") next_assigned_task_id: String): Call<StartJobResponse>

    @Multipart
    @POST("api/partiallyDonedrops")
    abstract fun partiallyDoneDeliverysAPI(@Part file: MultipartBody.Part,@Part("type") type: RequestBody,
                                           @Part("status") status: RequestBody,
                                           @Part("id") id: RequestBody,
                                           @Part("assigned_task_id") assigned_task_id: RequestBody,
                                           @Part("next_assigned_task_id") next_assigned_task_id: RequestBody,
                                           @Part("delivery_location") delivery_location: RequestBody,
                                           @Part("delivery_date") delivery_date: RequestBody
                                           ): Call<StartJobResponse>


    @FormUrlEncoded
    @POST("api/updateSheets")
    abstract fun updateSheets(@Field("assigned_id") assigned_id: String, @Field("used_sheets") used_sheets: String): Call<StartJobResponse>

    @FormUrlEncoded
    @POST("api/getDepartmentwiseCount")
    abstract fun getDepartmentwiseCount(@Field("user_id") user_id: String): Call<DepartmentListResponse>

    @FormUrlEncoded
    @POST("api/getPrinterWiseCounts")
    abstract fun getPrintertypesCount(@Field("user_id") user_id: String): Call<PrinterTypesListResponse>

    /* @FormUrlEncoded
     @POST("/Mobile/addresses")
     abstract fun getAddresses(@Header("encryption_key") encrypt_key: String,
                               @Field("user_id") user_Id: String): Call<AddressesResponse>

     @FormUrlEncoded
     @POST("/Mobile/addAddress")
     abstract fun addAddresses(@Header("encryption_key") encrypt_key: String,
                               @Field("user_id") user_id: String,
                               @Field("name") name: String,
                               @Field("company_name") company_name: String,
                               @Field("address1") address1: String,
                               @Field("address2") address2: String,
                               @Field("city") city: String,
                               @Field("state") state: String,
                               @Field("zipcode") zipcode: String): Call<AddAddressesResponse>

     @FormUrlEncoded
     @POST("/Mobile/updateAddress")
     abstract fun updateAddresses(@Header("encryption_key") encrypt_key: String,
                                  @Field("address_id") address_id: String,
                                  @Field("name") name: String,
                                  @Field("company_name") company_name: String,
                                  @Field("address1") address1: String,
                                  @Field("address2") address2: String,
                                  @Field("city") city: String,
                                  @Field("state") state: String,
                                  @Field("zipcode") zipcode: String): Call<UpdateAddressesResponse>


     @FormUrlEncoded
     @POST("/Mobile/deleteAddress")
     abstract fun deleteAddresses(@Header("encryption_key") encrypt_key: String,
                                  @Field("address_id") address_id: String,
                                  @Field("name") name: String,
                                  @Field("company_name") company_name: String,
                                  @Field("address1") address1: String,
                                  @Field("address2") address2: String,
                                  @Field("city") city: String,
                                  @Field("state") state: String,
                                  @Field("zipcode") zipcode: String): Call<DeleteAddressesResponse>


     @FormUrlEncoded
     @POST("/Mobile/updateProfile")
     abstract fun updateProfile(@Header("encryption_key") encrypt_key: String,
                                @Field("user_id") address_id: String,
                                @Field("first_name") name: String,
                                @Field("last_name") company_name: String,
                                @Field("company") address1: String,
                                @Field("phone_number") address2: String,
                                @Field("alternate_number") city: String,
                                @Field("customer_service_representative") state: String,
                                @Field("referred_by") zipcode: String): Call<UpdateProfileResponse>

     @FormUrlEncoded
     @POST("/Mobile/userProfile")
     abstract fun getProfile(@Header("encryption_key") encrypt_key: String,
                             @Field("user_id") user_id: String): Call<UpdateProfileResponse>


     @FormUrlEncoded
     @POST("/Mobile/login")
     abstract fun login(@Header("encryption_key") encrypt_key: String,
                        @Field("username") username: String,
                        @Field("password") password: String): Call<UserLoginResponse>

     @FormUrlEncoded
     @POST("/Mobile/changePassword")
     abstract fun changePassword(@Header("encryption_key") encrypt_key: String,
                                 @Field("user_id") user_id: String,
                                 @Field("old_password") old_password: String,
                                 @Field("new_password") new_password: String): Call<ChangePasswordResponse>

     @FormUrlEncoded
     @POST("/Mobile/forgotPassword")
     abstract fun forgotPassword(@Header("encryption_key") encrypt_key: String,
                                 @Field("email") email: String): Call<ChangePasswordResponse>


     @FormUrlEncoded
     @POST("/Mobile/forgotUser")
     abstract fun forgotUserName(@Header("encryption_key") encrypt_key: String,
                                 @Field("email") email: String): Call<ChangePasswordResponse>

     @FormUrlEncoded
     @POST("/Mobile/orders")
     abstract fun getUserOrders(@Header("encryption_key") encrypt_key: String,
                                @Field("user_id") user_id: String,
                                @Field("status") status: String): Call<UserOrderResponse>

     @FormUrlEncoded
     @POST("/Mobile/signup")
     abstract fun registerAccount(@Header("encryption_key") encrypt_key: String,
                                  @Field("first_name") name: String,
                                  @Field("last_name") company_name: String,
                                  @Field("username") user_name: String,
                                  @Field("email") email: String,
                                  @Field("company_name") address1: String,
                                  @Field("phone_number") address2: String,
                                  @Field("alternate_number") city: String,
                                  @Field("password") passwd: String,
                                  @Field("referred_by") zipcode: String,
                                  @Field("terms_conditions") terms: String): Call<RegisterResponse>

     @FormUrlEncoded
     @POST("/Mobile/orderView")
     abstract fun getOrderDetails(@Header("encryption_key") encrypt_key: String,
                                  @Field("order_id") order_id: String): Call<OrderDetailsResponse>

     @GET("/Mobile/states")
     abstract fun getStates(@Header("encryption_key") encrypt_key: String): Call<StatesResponse>


     @FormUrlEncoded
     @POST("/Mobile/getProfileList")
     abstract fun getProfileList(@Header("encryption_key") encrypt_key: String,
                                 @Field("user_id") user_id: String,
                                 @Field("address_type") address_type: String): Call<ProfileListResponse>

     *//*@Multipart
    @POST("/Mobile/fileUpload")
    fun fileUpload(@Header("encryption_key") encrypt_key: String, @Part(MultipartBody) Mufile_name filePart): Call<FileUploadResponse>*//*

    @Multipart
    @POST("/Mobile/fileUpload")
   *//* fun fileUpload(
            @Header("encryption_key") encrypt_key: String,
            @Part file_name: MultipartBody.Part): Call<FileUploadResponse>*//*

    abstract fun fileUpload(@Header("encryption_key") encrypt_key: String,@Part file: MultipartBody.Part): Call<FileUploadResponse>*/


    companion object Factory {
        val BASE_URL = "http://18.217.209.201/graceprintinternal/"


        val client = OkHttpClient.Builder()
                .connectTimeout(10,TimeUnit.SECONDS)
                .readTimeout(30,TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client.build())
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}