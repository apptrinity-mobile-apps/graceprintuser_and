package indo.com.graceprinting.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class StartJobListDataResponse {

    val assigned_task_id: String? = null
    val job_task_id: String? = null
    val assigned_id: String? = null
    val task_start_date: String? = null
    val task_end_date: String? = null
    val assigned_task_status: String? = null
    val task_name: String? = null
    val delivery_sign: String? = null
    val done_status: String? = null

    val comments: Array<CommentsListDataResponse>? = null

    ///new
    var partiallyarr:Array<PartiallydonDataArray>?=null
    var dropsarr:Array<Dropsarraylist>?=null
}
