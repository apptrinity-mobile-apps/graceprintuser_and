package indo.com.graceprinting.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DepartmentListResponse {
     val result: String? = null

     val data: Array<DepartmentListDataResponse>? = null

     val status: String? = null


}
