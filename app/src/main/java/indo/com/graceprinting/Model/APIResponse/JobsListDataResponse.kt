package indo.com.graceprinting.Model.APIResponse

class JobsListDataResponse {

    val id: String? = null
    val job_id: String? = null
    val department_id: String? = null
    val job_description: String? = null

    val job_department_id: String? = null
   // val user_id: String? = null
    val job_part: String? = null
    val planeed_activity: String? = null
    val earliest_start_date: String? = null
    val custmer: String? = null
    val description: String? = null
    val promise_date: String? = null
    val earliest_start_time: String? = null
    val status: String? = null
    val scheduled: String? = null
    val crit_start_date: String? = null
    val due_time: String? = null
    val last_act_code: String? = null
    val created_date: String? = null
    val assigned_id: String? = null
    val assigned_status: String? = null
    val assigned_task_id: String? = null
    val alloted_sheets: String? = null
    val used_sheets: String? = null
    val prepress_status: String? = null
    val press_status: String? = null
    val bindery_status: String? = null
    val mailing_status: String? = null
    val logistics_status: String? = null
    val start_status: String? = null
    val job_title: String? = null

   val size_id:String?=null
   val printer_type:String?=null
   val option_type:String?=null
   val paper_name:String?=null
   val text_name:String?=null
   val sub_text_name:String?=null
   val weight:String?=null
   val width:String?=null
   val height:String?=null
   val mailing_qty:String?=null
   val no_of_terms:String?=null


 val terms: Array<String>? = null
   val term_date: Array<String>? = null

}
