package indo.com.graceprinting.Model.APIResponse

class StartJobResponse {

    val data: Array<StartJobListDataResponse>? = null

    val result: String? = null

    val status: String? = null

    val next_assigned_task_id: String? = null



}
