package indo.com.graceprinting.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class DepartmentListDataResponse {

     val department_name: String? = null
     val upcoming: String? = null
     val active: String? = null
     val total: String? = null
     val department_id: String? = null




}
