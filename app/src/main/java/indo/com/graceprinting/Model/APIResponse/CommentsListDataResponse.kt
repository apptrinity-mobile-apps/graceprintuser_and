package indo.com.graceprinting.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class CommentsListDataResponse {

    val comment_id: String? = null
    val assigned_task_id: String? = null
    val comment: String? = null
    val user_type: String? = null
    val user_id: String? = null
    val comment_on: String? = null

}
