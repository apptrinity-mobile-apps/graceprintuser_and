package indo.com.graceprinting.Model.APIResponse

class GetJobsResponse {
    val data: Array<JobsListDataResponse>? = null

    val result: String? = null

    val status: String? = null
}
