package indo.com.graceprinting.Model.APIResponse

class Dropsarraylist {


    var id:String?=null
    var job_id:String?=null
    var term:String?=null
    var date:String?=null
    var term_status:String?=null
    var mailing_status:String?=null
    var shipping_status:String?=null
    var delivery_status:String?=null
    var delivery_sign:String?=null
    var delivery_date:String?=null
    var delivery_location:String?=null
    val last_done_status: String? = null


}