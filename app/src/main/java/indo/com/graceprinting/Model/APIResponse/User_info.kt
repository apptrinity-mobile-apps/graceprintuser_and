package indo.com.graceprinting.Model.APIResponse

import com.fasterxml.jackson.annotation.JsonIgnoreProperties


@JsonIgnoreProperties(ignoreUnknown = true)
class User_info {

    val id: String? = null
    val department_id: String? = null
    val department_name: String? = null
    val user_name: String? = null
    val user_email: String? = null
    val user_password: String? = null
    val user_mobile_number: String? = null
    val status: String? = null
    val created_date: String? = null


}
