package indo.com.graceprinting.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import indo.com.graceprinting.Fragment.ActiveFragment
import indo.com.graceprinting.Fragment.AllFragment
import indo.com.graceprinting.Fragment.CompletedFragment
import indo.com.graceprinting.Fragment.UpcomingFragment


class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                AllFragment()
            }
            1 -> ActiveFragment()
            2 -> UpcomingFragment()
            else -> {
                return CompletedFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "ALL"
            1 -> "ACTIVE"
            2 -> "UPCOMING"
            else -> {
                return "COMPLETED"
            }
        }
    }
}