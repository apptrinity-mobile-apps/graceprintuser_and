package indo.com.graceprinting.Adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import indo.com.graceprinting.Activities.MainActivity
import indo.com.graceprinting.Model.APIResponse.DepartmentListDataResponse
import indo.com.graceprinting.R


class DepartmentListAdapter(val mServiceList: ArrayList<DepartmentListDataResponse>, val context: Context) : RecyclerView.Adapter<ViewHolder1>() {

    private val list_images = intArrayOf(R.drawable.prepress_icon, R.drawable.press_icon, R.drawable.bindery_icon, R.drawable.mailing_icon, R.drawable.shipping_icon, R.drawable.delivery_icon)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
        return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.deptment_listitem, parent, false))


    }

    override fun onBindViewHolder(holder: ViewHolder1, position: Int) {


        myloading()

        val imagePos = position % list_images.size

        Log.e("", "")
        holder?.tv_deptname.text = mServiceList.get(position).department_name
        holder?.tv_deptcount.text = "(" + mServiceList.get(position).total + ")"
        holder?.tv_upcoming_count.text = mServiceList.get(position).upcoming
        holder?.tv_active_count.text = mServiceList.get(position).active


        //holder?.iv_deplistitem.setBackgroundResource(list_images[imagePos])


        if (mServiceList.get(position).department_name.equals("Prepress")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.prepress_icon)
        } else if (mServiceList.get(position).department_name.equals("Press")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.press_icon)
        } else if (mServiceList.get(position).department_name.equals("Bindery")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.bindery_icon)
        } else if (mServiceList.get(position).department_name.equals("Mailing")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.mailing_icon)
        } else if (mServiceList.get(position).department_name.equals("Shipping")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.shipping_icon)
        } else if (mServiceList.get(position).department_name.equals("Delivery")) {
            holder?.iv_deplistitem.setImageResource(R.drawable.delivery_icon)
        }


        holder?.cv_deptcount.setOnClickListener(View.OnClickListener {
            val intent = Intent(this.context, MainActivity::class.java)
            intent.putExtra("department_id", mServiceList.get(position).department_id)
            intent.putExtra("department_name", mServiceList.get(position).department_name)
            intent.putExtra("printerid", "")
            context.startActivity(intent)
        })


    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return mServiceList.size
    }

    // Inflates the item views


    internal lateinit var my_loader: Dialog
    private fun myloading() {
        my_loader = Dialog(this.context)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false);
        my_loader.setContentView(R.layout.mkloader_dialog)
    }

}

class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val cv_deptcount = view.findViewById<CardView>(R.id.cv_deptcount)
    val tv_deptname = view.findViewById(R.id.tv_deptname) as TextView
    val tv_deptcount = view.findViewById(R.id.tv_deptcount) as TextView
    val tv_upcoming_count = view.findViewById(R.id.tv_upcoming_count) as TextView
    val tv_active_count = view.findViewById(R.id.tv_active_count) as TextView
    val iv_deplistitem = view.findViewById(R.id.iv_deplistitem) as ImageView
}



